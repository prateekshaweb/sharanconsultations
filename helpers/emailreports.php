<?php
namespace sharanconsultation;

use \radient\Radient_Classes_Postmeta;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Active Tasks
 * Model
 * Template
 * Parsing
 * Send
 *
 * Add title
 * Sorting Date desc
 * Header - Company
 * Date....
 *
 *
 **/

class Sharanconsultation_Helpers_Emailreports
{

    public static function sendSummaryReport()
    {

        $html = self::getMainTemplate();

        $html = self::getSummaryPosttype(array('posttype' => 'tasks', 'boxtitle' => 'Tasks2'));
        $html = self::getProjectSummary(array('posttype' => 'projects', 'boxtitle' => 'Active Project'));
        $html = self::getProjectSummary(array('posttype' => 'activities', 'boxtitle' => 'Active Activity'));
        $html = self::getProjectSummary(array('posttype' => 'opportunities', 'boxtitle' => 'Active Opportunities'));
        $html = self::getNewContacts(array('posttype' => 'contacts'));

    }

    public static function getMainTemplate()
    {

        /**
         * add header part
         */
        $instance = Sharanconsultation_Init();

        $logo = (int) $instance->getOption('logo');
        if ($logo) {
            echo wp_get_attachment_image($logo, 'full', $size = '85px');
        }
        ?>
        <div align="center" style="width: 400px; margin: 0 auto; font-size: 12px; padding-top: 5px;">
                <span style="font-size: 21px; border-bottom: 1px solid black; padding-bottom: 2px;"><?php echo $instance->getOption('company_name'); ?></span><br />
            <div style="padding-top: 5px;">
        <?php
$title = $instance->getOption('organization');
        $address = $instance->getOption('address_2');
        $street = $instance->getOption('street');
        $city = $instance->getOption('city');
        $postcode = $instance->getOption('postcode');
        $state = $instance->getOption('state');
        $country = $instance->getOption('country');
        $phone = $instance->getOption('mob_no');
        $email = $instance->getOption('email');
        $template_head = "
                        <table>
                            <tr><td align='center'><h1>{{title}}</h1></td></tr>
                            <tr><td align='center'>{{address}}</td></tr>
                            <tr><td align='center'>{{street}}{{city}} {{postcode}} {{state}} {{country}}</td></tr>
                            <tr><td align='center'>Contact info : {{phone}}</td></tr>
                            <tr><td align='center'>Email : {{email}}</td></tr>
                        </table>";

        // Twig
        include_once SHARANCONSULTATION_DIR . 'app' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Twig/Autoloader.php';
        \Twig_Autoloader::register();
        $loader = new \Twig_Loader_Array(array('index.html' => $template_head));
        $twig = new \Twig_Environment($loader);

        echo $twig->render('index.html', array('title' => $title, 'address' => $address, 'street' => $street, 'city' => $city, 'postcode' => $postcode, 'country' => $country, 'phone' => $phone, 'email' => $email));

        ?>
            </div>
    </div><!-- main -->
        <?php
}

    public static function getSummaryPosttype($args = array())
    {
        // FOR ACTIVE TASKS
        $default = array(
            'posttype' => 'tasks',
            'boxtitle' => 'Tasks',
            'status' => 'active',
        );
        $args = wp_parse_args($args, $default);
        extract($args);

        // Get the model
        $model = Sharanconsultation_Init()->getModel($posttype);
        $model->setMetaValue('status', $status);
        // Rows
        $rows = $model->getList();

        foreach ($rows as &$row) {
            $row->start_date = Sharanconsultation_Helpers_Common::getDateFromPostId($row->ID, 'start_date');
            $row->status = Radient_Classes_Postmeta::fetch($row->ID, 'status');
            $row->project_title = get_the_title(Radient_Classes_Postmeta::fetch($row->ID, 'project_id'));
            $row->customer = strip_tags(Sharanconsultation_Helpers_Common::getNameFromAccountId($row->ID));
        }

        $template_text = "
            <table style ='border: 1px solid black;  width: 100%;'>
                    <tr>
                        <th style ='border: 1px solid black;'>Title</th>
                        <th style ='border: 1px solid black;'>Project Name</th>
                        <th style ='border: 1px solid black;'>Date</th>
                        <th style ='border: 1px solid black;'>Status</th>
                        <th style ='border: 1px solid black;'>Customer</th>
                    </tr>
            {% for row in rows %}
                    <tr>
                        <td style ='border: 1px solid black;'>{{row.post_title}}</td>
                        <td style ='border: 1px solid black;'>{{row.project_title}}</td>
                        <td style ='border: 1px solid black;'>{{row.start_date}}</td>
                        <td style ='border: 1px solid black;'>{{row.status}}</td>
                        <td style ='border: 1px solid black;'>{{row.customer}}</td>
                    </tr>
            {% endfor %}
                </table>";

        // Twig
        include_once SHARANCONSULTATION_DIR . 'app' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Twig/Autoloader.php';
        \Twig_Autoloader::register();
        $loader = new \Twig_Loader_Array(array('index.html' => $template_text));
        $twig = new \Twig_Environment($loader);

        echo '<h2>' . $args['boxtitle'] . '</h2>';

        echo $twig->render('index.html', array('rows' => $rows));

    }

    public static function getProjectSummary($args = array())
    {

        //  FOR ACTIVE PROJECT
        $default2 = array(
            'posttype' => $args['posttype'],
            'boxtitle' => $args['boxtitle'],
            'status' => 'active',
        );
        $args = wp_parse_args($args, $default2);
        extract($args);
// Get the model
        $model = Sharanconsultation_Init()->getModel($posttype);
        $model->setMetaValue('status', $status);
// Rows
        $rows = $model->getList();

        foreach ($rows as &$row) {
            $row->start_date = Sharanconsultation_Helpers_Common::getDateFromPostId($row->ID, 'start_date');
            $row->status = Radient_Classes_Postmeta::fetch($row->ID, 'status');
            $row->project_title = get_the_title(Radient_Classes_Postmeta::fetch($row->ID, 'project_id'));
            $row->customer = strip_tags(Sharanconsultation_Helpers_Common::getNameFromAccountId($row->ID));
        }

        $template_text = "
            <table style ='border: 1px solid black;  width: 100%;'>
                    <tr>
                        <th style ='border: 1px solid black;'>Title</th>
                        <th style ='border: 1px solid black;'>Date</th>
                        <th style ='border: 1px solid black;'>Status</th>
                        <th style ='border: 1px solid black;'>Customer</th>
                    </tr>
            {% for row in rows %}
                    <tr>
                        <td style ='border: 1px solid black;'>{{row.post_title}}</td>
                        <td style ='border: 1px solid black;'>{{row.start_date}}</td>
                        <td style ='border: 1px solid black;'>{{row.status}}</td>
                        <td style ='border: 1px solid black;'>{{row.customer}}</td>
                    </tr>
            {% endfor %}
                </table>";
        $loader = new \Twig_Loader_Array(array('index.html' => $template_text));
        $twig = new \Twig_Environment($loader);

        echo '<h2>' . $args['boxtitle'] . '</h2>';

        echo $twig->render('index.html', array('rows' => $rows));
    }

/***
 *
 *
 */

    public static function getNewContacts($args = array())
    {
        // FOR  new contact

        $default = array(
            'posttype' => 'contacts',
            'boxtitle' => 'New Contacts',
            'meta_query' => array(
                array(
                    'key' => 'contact_date',
                    // value should be array of (lower, higher) with BETWEEN
                    'value' => array(date('M-d-20y', strtotime("-1 days")), date('M-d-20y')),
                    'compare' => 'BETWEEN',
                    'type' => 'DATE',
                ),
            ),
        );

        $args = array(
            'post_type' => 'crmcontacts',
            'orderby' => 'meta_value',
            'meta_query' => array(
                array(
                    'key' => 'contact_date',
                    // value should be array of (lower, higher) with BETWEEN
                    'value' => array(date('Ymd', strtotime("-1 days")), date('Ymd')),
                    'compare' => 'BETWEEN',
                    'type' => 'DATE',
                ),
            ),
        );

        // Rows
        $rows = new \WP_Query($args);
         while ($rows->have_posts()) {

            $rows->the_post();
            $post_id = get_the_ID();

            $rows->start_date = Sharanconsultation_Helpers_Common::getDateFromPostId($post_id, 'start_date');
            $rows->status = Radient_Classes_Postmeta::fetch($post_id, 'status');
            $rows->project_title = get_the_title(Radient_Classes_Postmeta::fetch($post_id, 'project_id'));
            $rows->customer = strip_tags(Sharanconsultation_Helpers_Common::getNameFromAccountId($post_id));
        }

        $template_text = "<h2> New Contacts </h2>
            <table style ='border: 1px solid black;  width: 100%;'>
                    <tr>
                        <th style ='border: 1px solid black;'>Title</th>
                        <th style ='border: 1px solid black;'>Project Name</th>
                        <th style ='border: 1px solid black;'>Date</th>
                        <th style ='border: 1px solid black;'>Status</th>
                        <th style ='border: 1px solid black;'>Customer</th>
                    </tr>
            {% for row in rows %}
                    <tr>
                        <td style ='border: 1px solid black;'>{{rows.post_title}}</td>
                        <td style ='border: 1px solid black;'>{{rows.project_title}}</td>
                        <td style ='border: 1px solid black;'>{{rows.start_date}}</td>
                        <td style ='border: 1px solid black;'>{{rows.status}}</td>
                        <td style ='border: 1px solid black;'>{{rows.customer}}</td>
                    </tr> 
            {% endfor %}
                </table>";

        // Twig
        include_once SHARANCONSULTATION_DIR . 'app' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Twig/Autoloader.php';
        \Twig_Autoloader::register();
        $loader = new \Twig_Loader_Array(array('index.html' => $template_text));
        $twig = new \Twig_Environment($loader);


        echo $twig->render('index.html', array('rows' => $rows));

    }

}
