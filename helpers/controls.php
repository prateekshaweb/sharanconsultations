<?php

/**
 * Class for Controls
 *
 * @category Helpers
 * @package sharanRadient
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 *      
 */

namespace sharanconsultation;
use \radient\Radient_Helpers_Controls;

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class Sharanconsultation_Helpers_Controls
 *      
 */
class Sharanconsultation_Helpers_Controls extends Radient_Helpers_Controls
{
    /**
     * Method to get the input
     * 
     * @param string $name
     * 
     * @return object
     */
    public static function getInput($name)
    {
        /** If empty then set to default  */
        $name = sanitize_key(trim($name));
        
        // Get the namespace
        $classname = __NAMESPACE__. '\Sharanconsultation_Helpers_Types_'.ucfirst($name);
        if ( class_exists($classname) ) {
            return new $classname;    
        }

        return Radient_Helpers_Controls::getInput($name);
    }
    
		
}
