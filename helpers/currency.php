<?php

/**
 * Class for Settings
 *
 * @category Core
 * @package sharanDailyLog
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 *      
 */
namespace sharanconsultation;

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class sharanConsultation
 *
 * @category Tasks
 * @package sharanconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *      
 */
class Sharanconsultation_Helpers_Currency extends \radient\Radient_Helpers_Currency
{
	
	static $_namespace = __NAMESPACE__;
	
		
}
