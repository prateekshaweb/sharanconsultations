<?php
/**
 *
 * @package sharanConsultation
 * @author  Prateeksha Team
 * @version 1.0.0
 * @license GPL-3.0+
 * @copyright  2002-2017, Prateeksha Team
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for main App for Employeesdb
 *
 * @category Core
 * @package radient
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 *
 */

class Sharanconsultation_Helpers_Misc
{

    /**
     * Set the namespace
     */
    static $_namespace = __NAMESPACE__;

    /**
     * Adds "Import" button on module list page
     *
     */
    public static function addViewButton()
    {

        global $current_screen;

        $allowed_posttypes = array('sharanconsultation');

        if (!in_array($current_screen->post_type, $allowed_posttypes)) {
            return;
        }

        $request = Sharanconsultation_Init()->request;
        if ('edit' != $request->get('action')) {
            return;
        }

        $post_id = $request->get('post', 0, 'integer');
        if (!$post_id) {
            return;
        }

        $escape = Sharanconsultation_Init()->escape;

        $url = add_query_arg(array(
            'action' => 'viewcrm',
            'type' => 'thickbox',
            'post_type' => $current_screen->post_type,
            'post_id' => $post_id,
            'TB_iframe' => "true",
        ));

        ?>
        <style>
        #TB_window {
            min-width:60% !important;
            margin-left: -30% !important;
            margin-top: -200px !important;
        }

        #TB_ajaxContent {
            min-width: 100% !important;
            min-height: 525px !important;
        }
        </style>
        <?php

        ob_start();
        ?>
            jQuery(jQuery(".wp-heading-inline")[0]).append( " &nbsp; &nbsp;<a href='<?php echo $escape->toUrl($url); ?>'  id='doc_popup' class='button thickbox  add-new-h2'>View</a>" );
        <?php
$script = ob_get_clean();
        \radient\Radient_Helpers_Common::addInlineOnloadjQuery($script);

        add_thickbox();
    }

    /**
     * Method to render a form for sending task information
     * You have to send information to Members, and Customers, or any third party
     *
     * @return NULL
     */
    public static function popupView()
    {
        // Helper
        $request = Sharanconsultation_Init()->request;

        // Get Action
        $action = $request->get('action', null, 'cmd');
        if ($action != 'viewcrm' && $action != 'emailcrm') {
            return "error";
        }

        $post_id = $request->get('post_id', null, 'integer');
        if (!$post_id) {
            return "error";
        }

        $post = get_post($post_id);
        if (!$post) {
            return "error";
        }

        ob_start();

        $template = Sharanconsultation_Init()->template;
        $template->clear();
        $template->post = $post;
        $result = $template->load('posttypes', $post->post_type . '.tpl.php');

        if (is_wp_error($result)) {
            die(__('Error'));
        }

        $message = ob_get_clean();
        echo $message;
        exit();
    }

    public static function admin_menu()
    {
        $instance = \radient\Init::get_instance(__NAMESPACE__);
        $menus = $instance->configmanager->get('adminmenu');

        // Check if array
        if (is_array($menus)) {

            // Iterate
            foreach ($menus as $menu) {

                $default = array(
                    array(
                        'menu_type' => 'menu_page',
                        'parent_slug' => null,
                        'page_title' => null,
                        'menu_title' => null,
                        'capability' => 'manage_options',
                        'menu_slug' => null,
                        'callback_function' => null,
                    ),
                );
                $menu = wp_parse_args($menu, $default);
                extract($menu);

                if ($menu_type == 'menu_page') {
                    add_menu_page(
                        __($page_title, 'sharanconsultation'),
                        __($menu_title, 'sharanconsultation'),
                        $capability,
                        $menu_slug,
                        $callback_function
                    );
                } else {
                    add_submenu_page(
                        $parent_slug,
                        __($page_title, 'sharanconsultation'),
                        __($menu_title, 'sharanconsultation'),
                        $capability,
                        $menu_slug,
                        $callback_function);
                }
            }
        }
    }

    /**
     * Block comment
     *
     * @return void
     */
    public static function load_language()
    {
        $languages_dir = trailingslashit(SHARANCONSULTATION_DIR . DIRECTORY_SEPARATOR . 'languages');
        load_plugin_textdomain('sharanconsultation', false, $languages_dir);
    }

}