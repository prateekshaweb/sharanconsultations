<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Mailfolder extends \radient\Radient_Classes_Taxonomy
{

	/**
	 * 
	 */
    static $TAXONOMY_NAME = 'mailfolder';

    static $POSTTYPE = 'crmmessages';

	/**
	 * 
	 */
    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
            'slug' => 'mailfolder',
            ),
        );

        parent::registerTaxonomy(_('Mail Folder'), _('Mail Folder'), $args);
    }
}
