<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Opportunitystage extends \radient\Radient_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmopportunitiesstagestage';

    static $POSTTYPE = 'crmopportunities';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmopportunitiesstagestage',
            ),
        );

        parent::registerTaxonomy(_('Opportunity Stage'), _('Opportunity Stage'), $args);
    }
}
