<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Contactstatus extends \radient\Radient_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmcontactsstatus';

    static $POSTTYPE =  'crmcontacts';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmcontactsstatus',
            ),
        );

        parent::registerTaxonomy(_('Contact Status'), _('Contact Status'), $args);
    }
}
