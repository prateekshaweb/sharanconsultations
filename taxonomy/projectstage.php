<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Projectstage extends \radient\Radient_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmprojectsstage';

    static $POSTTYPE = 'crmprojects';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmprojectsstage',
            ),
        );

        parent::registerTaxonomy(_('Project Stage'), _('Project Stage'), $args);
    }
}
