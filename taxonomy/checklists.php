<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Checklists extends \radient\Radient_Classes_Taxonomy
{

	/**
	 * 
	 */
    static $TAXONOMY_NAME = 'checklists';

    static $POSTTYPE = 'crmtasks';

	/**
	 * 
	 */
    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'checklists',
            ),
        );

        parent::registerTaxonomy(_('Checklists'), _('Checklists'), $args);
    }
}
