<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Productstatus extends \radient\Radient_Classes_Taxonomy
{


    static $TAXONOMY_NAME = 'productstatus';

    static $POSTTYPE = 'rdsproducts';

    public static function register()
    {
        $args = array(
            'public' => false,
            'publicly_queryable' => true,
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'productstatus',
            ),
        );

        parent::registerTaxonomy(_('Status'), _('Status'), $args);

    }
}
