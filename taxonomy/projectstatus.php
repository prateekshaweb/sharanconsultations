<?php

namespace sharanconsultation;

/**
 */
class Sharanconsultation_Taxonomy_Projectstatus extends \radient\Radient_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmprojectsstatus';

    static $POSTTYPE = 'crmprojects';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmprojectsstatus',
            ),
        );

        parent::registerTaxonomy(_('Project Status'), _('Project Status'), $args);
    }
}
