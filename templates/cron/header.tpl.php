<?php

/**
 * Class for Accounts Post Type
 * Accounts is used for Products, Services etc
 *
 * PHP Version 5.6
 *
 * @category  Posttype
 * @package   sharanConsultation
 * @author    Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license   see license.txt
 * @link      http://www.prateeksha.com/sharanconsultation/
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

?>
<html>
<head>
<style>
body {
	background-color: #ccc;
}
</style>
</head>
<body>
        <table style="width: 650px; background-color: white; margin: auto;">
                <tr>
                        <td><h1><?php _e('Ticketing  Reports'); ?></h1>
              <?php _e('For '); 
$instance = Sharanconsultation_Init();
$title = $instance->getOption('organization');
$address = $instance->getOption('address_2');
$street = $instance->getOption('street');
$city = $instance->getOption('city');
$postcode = $instance->getOption('postcode');
$state = $instance->getOption('state');
$country = $instance->getOption('country');
$phone = $instance->getOption('mob_no');
$email = $instance->getOption('email');

echo $instance->getOption('f_name') . " " . $instance->getOption('l_name');
 _e('  on -');
echo date('l') . "  " . date("d M Y");
?>
		</td>
                </tr>
                <tr>
                        <td><h3><?php echo $title; ?></h3></td>
                </tr>
                <tr>
                        <td><?php echo $address; ?></td>
                </tr>
                 <tr>
                        <td><?php echo $street . " " . $city . "- " . $postcode; ?></td>
                </tr>
                  <tr>
                        <td><?php echo $state . " " . $country; ?></td>
                </tr>
                <tr>
                        <td><?php echo "Phone: ".$phone . " Email: " . $email; ?></td>
                </tr>

                <tr>
                        <td>