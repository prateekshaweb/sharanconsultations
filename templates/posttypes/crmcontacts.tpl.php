<?php

/**
 * Prateeksha_sharanPM - Project Management
 *
 * @category Tasks
 * @package Prateeksha_sharanPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

$defaults = array(
    'company' => array(
        'not Mentioned',
    ),
    'first_name' => array(
        'Not mentioned',
    ),
    'last_name' => array(
        'Not mentioned',
    ),
    'phone' => array(
        'Not mentioned',
    ),
        'job_title' => array(
        'Not mentioned',
    ),
    'mobile' => array(
        'Not mentioned',
    ),
    'email_id' => array(
        'Not mentioned',
    ),
    'secondary_email' => array(
        'Not mentioned',
    ),
    'fax' => array(
        'Not mentioned',
    ),
    'home_phone'=> array(
        'Not mentioned',
    ),
    'website' => array(
        'Not proivded',
    ),

    'account_site' => array(
        'Not mentioned',
    ),
    'account_number' => array(
        'Not mentioned',
    ),
    'annual_revenue' => array(
        'Not mentioned',
    ),

    'no_of_employees' => array(
        'Not mentioned',
    ),
    'sic_code' => array(
        'Not mentioned',
    ),
    'ticker_code' => array(
        'Not mentioned',
    ),
    'skype_id' => array(
        'Not mentioned',
    ),
    'twitter_id' => array(
        'Not mentioned',
    ),
    'description' => array(
        'Not mentioned',
    ),
    'mailing_street' => array(
        '',
    ),
    'mailing_postcode' => array(
        '',
    ),
    'mailing_state' => array(
        '',
    ),
    'mailing_address' => array(
        'Not mentioned',
    ),
    'mailing_city' => array(
        '',
    ),
    'mailing_state' => array(
        '',
    ),
    'mailing_country' => array(
        '',
    ),

    'other_street' => array(
        '',
    ),
    'other_postcode' => array(
        '',
    ),
    'other_state' => array(
        '',
    ),
    'other_address' => array(
        'Not mentioned',
    ),
    'other_city' => array(
        '',
    ),
    'other_state' => array(
        '',
    ),
    'other_country' => array(
        '',
    ),
     'assistant_name' => array(
        'Not mentioned',
    ),
    'assistant_phone' => array(
        'Not mentioned',
    ),
    'lead_source' => array(
        'Not mentioned',
    ),
    'url' => array(
        'Not mentioned',
    ),
    'department' => array(
        'Not mentioned',
    ),
    'dob' => array(
        'Not mentioned',
    ),
    'rating' => array(
        'Not mentioned',
    ),
);
$postmeta = wp_parse_args(get_post_meta($this->post->ID), $defaults);

$instance = Sharanconsultation_Init();
?>
<style>
body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}
</style>
<div style="font-size: 15px; width: 850px; margin: 0px auto 0 auto; padding-top: 30px;">
   <center>
      <table style="width: 300px; margin: 0 auto; font-size: 18px; padding-bottom: 10px; text-align: center;">
         <tr>
            <td align="center">
        	<?php
$logo = (int) $instance->getOption('logo');
if ($logo) {
    echo wp_get_attachment_image($logo, 'full', $size = '85px');
}
?>
<div align="center" style="width: 400px; margin: 0 auto; font-size: 12px; padding-top: 5px;">
        <span style="font-size: 21px; border-bottom: 1px solid black; padding-bottom: 2px;"><?php echo $instance->getOption('organization'); ?></span><br />
        <div style="padding-top: 5px;"> <?php
$address = $instance->getOption('address');
if (!empty($address)) {
    echo $address . '<br/>';
}
$street = $instance->getOption(
    'street');
if (!empty($street)) {
    echo $street . '<br/>';
}
$city = $instance->getOption('city');
if (!empty(
    $city)) {
    echo $city .
        ' - ';
}
$this->postcode = $instance->getOption(
    'postcode');
if (!empty(
    $this->postcode)) {
    echo $this->postcode .
        ' ';
}
$country = $instance->getOption(
    'country');
if (!empty(
    $country)) {
    echo $country .
        '<br/>';
}
$phone = $instance->getOption(
    'phones');
if (!empty(
    $phone)) {
    echo __(
        'Tel : ') .
        $phone .
        '<br/>';
}
$email = $instance->getOption(
    'email');
if (!empty(
    $email)) {
    echo __(
        'Email : ') .
        $email .
        '<br/>';
}
$url = $instance->getOption(
    'url');
if (!empty(
    $email)) {
    echo __(
        'Web : ') .
        $url .
        '<br/>';
}
?></div>
                                        </div>
                                </td>
                        </tr>
                </table>
        </center>
        <div>
         <center>
                <h2 align="center" style="color: black;"><?php echo __('Contacts Report'); ?></h2>
         </center>
        <table cellspacing="13" style="border: 1px solid black;" width="100%" >
            <tbody>
                <tr>
                    <td>
                        <h4><?php echo __('Company Name'); ?>  :  <?php echo get_post_meta($this->post->ID, 'company', true); ?></h4>
                    </td>
                    <td>
                        <h4><?php echo __('Name'); ?> : <?php echo get_post_meta($this->post->ID, 'first_name', true); ?>  <?php echo get_post_meta($this->post->ID, 'last_name', true); ?></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo __('Job Title'); ?> : <?php echo $postmeta['job_title'][0]; ?>
                    </td>
                    <td><?php echo __('Mobile'); ?>  : <?php echo $postmeta['mobile'][0]; ?>
                    </td>
                          </tr>
                <tr>
                    <td>
                        <?php echo __('Phone'); ?> : <?php echo $postmeta['phone'][0]; ?>
                    </td>
                    <td><?php echo __('Home Phone'); ?>  : <?php echo $postmeta['home_phone'][0]; ?>
                    </td>
                          </tr>
                          <tr>
                         <td>
                         <?php echo __('Email'); ?>  : <?php echo $postmeta['email_id'][0]; ?>
                    </td>
                    <td> <?php echo __('secondry Email'); ?>  : <?php echo $postmeta['secondary_email'][0]; ?>
                    </td>
                          </tr>
                          <tr>
                         <td>
                       <?php echo __('Fax '); ?>: <?php echo $postmeta['fax'][0]; ?>
                    </td>
                    <td> <?php echo __('Website : '); ?> <?php echo $postmeta['website'][0]; ?>
                    </td>
                          </tr>
                          <tr>
                         <td>
                        <h4><?php echo __('Other Details'); ?> </h4>
                    </td>
                    <td>
                    </td>
                          </tr>
                           <tr>
                         <td>
                        <?php echo __('Assistant Name'); ?> : <?php echo $postmeta['assistant_name'][0]; ?>
                    </td>
                    <td> <?php echo __('Assistant Number'); ?> : <?php echo $postmeta['assistant_phone'][0]; ?>
                    </td>
                          </tr>
                          <tr>
                         <td>
                        <?php echo __('Lead Source'); ?> : <?php echo $postmeta['lead_source'][0]; ?>
                    </td>
                    <td> <?php echo __('URL'); ?> : <?php echo $postmeta['url'][0]; ?>
                    </td>
                          </tr>
                           <tr>
                         <td>
                       <?php echo __('Depatment'); ?> : <?php echo $postmeta['department'][0]; ?>
                    </td>
                    <td> <?php _e('Date of Birth ');
echo $postmeta['dob'][0];?>    </td>
                          </tr>
                           <tr>
                         <td>
                       <?php echo __('Rating'); ?> : <?php echo $postmeta['rating'][0]; ?>
                    </td>
                    <td>   </td>
                          </tr>
                           <tr>
                         <td>
                      <h4><?php echo __('Description'); ?> </h4>  </td>
                    <td></td>
                          </tr>
                          <tr>
                         <td colspan="2" >     <?php echo $postmeta['description'][0]; ?>           </td>

                          </tr>
                          <tr>
                         <td>
                      <h4><?php echo __('Address'); ?> </h4> <?php echo $postmeta['mailing_address'][0] . "</br> " . $postmeta['mailing_street'][0] . " </br>" . $postmeta['mailing_postcode'][0] . " " . $postmeta['mailing_city'][0] . "</br> " . $postmeta['mailing_state'][0] . " " . $postmeta['mailing_country'][0]; ?>  </td>
                    <td>
                        <h4><?php echo __('Secondary Address'); ?> </h4> <?php echo $postmeta['other_address'][0] . "</br> " . $postmeta['other_street'][0] . " </br>" . $postmeta['other_postcode'][0] . " " . $postmeta['other_city'][0] . "</br> " . $postmeta['other_state'][0] . " " . $postmeta['other_country'][0]; ?>  </td>
                    </td>
                          </tr>
                          <tr>
                         <td colspan="2" >        </td>

                          </tr>
                                </table>
                                <?php
