<?php

/**
 *
 * @package sharanConsultation
 * @author  Prateeksha Team
 * @version 1.0.0
 * @license GPL-3.0+
 * @copyright  2002-2017, Prateeksha Team
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for main App for Employeesdb
 *
 * @category Core
 * @package radient
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 *
 */
class Sharanconsultation_Init extends \radient\Radient_Init
{
    /**
     * Construct class
     * Sets the paths for template, css, js and icons, and helpers
     * Loads the posttypes, metaboxes
     * Registers the post types, meta boxes
     *
     * @since 1.0
     * 
     */
    protected function __construct()
    {
        // Set all the variables
        $this->_name = SHARANCONSULTATION_NAME;
        $this->_version = SHARANCONSULTATION_VERSION;
        $this->_slug = SHARANCONSULTATION_SLUG;
        $this->_namespace = __NAMESPACE__;        
        $this->_path = SHARANCONSULTATION_DIR;
        $this->_path_url = SHARANCONSULTATION_URI;
        $this->_options_name = SHARANCONSULTATION_OPTIONS_NAME;
        $this->_file = SHARANCONSULTATION_FILE;

        // Init the class
        parent::__construct();

        /*
        if (! wp_next_scheduled('crm_cron')) {
        wp_schedule_event(time(), 'hourly', 'crm_cron');}**/

        // Add View
        add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Core_Misc', 'popupView'));
        add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Adminpages_Options', 'register_settings'));
        add_action('edit_form_top', array($this, 'goBack'));
  add_action('current_screen', array('\sharanconsultation\Sharanconsultation_Core_Misc', 'addViewButton'));
       // add_action('current_screen', array($this, 'addViewButton'));

      //  add_action('edit_form_after_title', array('\sharanconsultation\Sharanconsultation_MetaBox_Messages_Details', 'moveDeck'));

        //add_action('admin_init', array( '\sharanconsultation\Sharanconsultation_Core_Cron', 'cronEmail'));

        // Metebox
       // add_action('init', array('\sharanconsultation\Sharanconsultation_MetaBox_Templates_Type', 'preview'));
    }


    
    /**
     * Method is used on activated
     * Adds the default values for statis
     *
     * @uses Sharanconsultation_Init::getOption
     * @uses wp_insert_term
     *
     * @return NULL
     */
    public function pluginActivation()
    {
        /**
         * Now add the taxonomy default values
         */
        $taxonomies = $this->getOption('default_taxonomy_values');
        foreach ($taxonomies as $taxonomy => $terms) {
            foreach ($terms as $key => $value) {
                $result = wp_insert_term(
                    $value,
                    $taxonomy,
                    array(
                        'slug' => strtolower($key),
                    )
                );
            }
        }

    }

    /**
     *
     */
    public static function goBack($post)
    {
		global $post;
        $allowed_posttypes = array('rdsproducts');
        if (!in_array($post->post_type, $allowed_posttypes)) {
            return;
        }
        
        $url = get_admin_url(null, 'edit.php?post_type=' . $post->post_type);
        echo '<a href="' . $url . '">Go Back</a>';
    }

    

    /**
     * Method to register the admin menus
     * Dashboard, Leads etc
     *
     * @uses add_menu_page, add_submenu_page
     *
     * @return void
     */
    public static function registerAdminMenu()
    {
        $menus = Sharanconsultation_Init()->settings->get('adminmenu');

        // Iterate
        foreach ($menus as $menu) {

            $default = array(
                array(
                    'menu_type' => 'menu_page',
                    'parent_slug' => null,
                    'page_title' => null,
                    'menu_title' => null,
                    'capability' => 'manage_options',
                    'menu_slug' => null,
                    'callback_function' => null,
                ),
            );
            $menu = wp_parse_args($menu, $default);
            extract($menu);

            if ($menu_type == 'menu_page') {
                add_menu_page(
                    __($page_title, 'sharanconsultation'),
                    __($menu_title, 'sharanconsultation'),
                    $capability,
                    $menu_slug,
                    $callback_function
                );
            } else {
                add_submenu_page(
                    $parent_slug,
                    __($page_title, 'sharanconsultation'),
                    __($menu_title, 'sharanconsultation'),
                    $capability,
                    $menu_slug,
                    $callback_function);
            }
        }

    }

    /**
     * Method to render a form for sending task information
     * You have to send information to Members, and Customers, or any third party
     *
     * @return NULL
     */
    public static function viewCrm()
    {
        // Helper
        $request = Sharanconsultation_Init()->request;

        // Get Action
        $action = $request->get('action', null, 'cmd');
        if ($action != 'viewcrm' && $action != 'emailcrm') {
            return "error";
        }

        $post_id = $request->get('post_id', null, 'integer');
        if (!$post_id) {
            return "error";
        }

        $post = get_post($post_id);
        if (!$post) {
            return "error";
        }

        ob_start();

        $template = Sharanconsultation_Init()->template;
        $template->clear();
        $template->post = $post;
        $result = $template->load('posttypes', $post->post_type . '.tpl.php');

        if (is_wp_error($result)) {
            die(__('Error'));
        }

        $message = ob_get_clean();
        echo $message;
        exit();
    }

    /**
     * Block comment
     *
     * @return void
     */
    public function loadLang()
    {
        parent::loadLang();

        // @todo check
        $languages_dir = trailingslashit(SHARANCONSULTATION_DIR . 'app' . DIRECTORY_SEPARATOR . 'languages');
        load_plugin_textdomain('sharanconsultation', false, $languages_dir);
    }

   
}

/** 
 * Main function to call the app
 */
function Sharanconsultation_Init()
{
    return Sharanconsultation_Init::getInstance(SHARANCONSULTATION_NAME);
}
