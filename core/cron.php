<?php

/**
 * Class for Billing Metabox
 * Used for Accounts Post Type etc
 *
 * PHP Version 5.6
 *
 * @category Metabox
 * @package sharanconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/*
 *
 * Class sharanConsultation
 *
 */
class Sharanconsultation_Core_Cron
{

    /**
     * Method to send cron email
     * We have a template for cron email and we can set everything there
     */
    public static function cronEmail()
    {
        // We are going to use PHP templates only
        ob_start();

        $template = Sharanconsultation_Init()->template;
        $template->clear();

        // Load the header
        $template->load('cron', 'header.tpl.php');

        // All the setting are there
        $cron_variables = (array) Sharanconsultation_Init()->settings->get('cron_settings');

        $escape = Sharanconsultation_Init()->escape;

        // There is a list of all the crons that needs to be run
        foreach ($cron_variables as $cron_variable) {
            // Almighty render
            echo self::renderCron(array(
                'post_type' => $escape->toSql($cron_variable['post_type']),
                'orderby' => $escape->toSql($cron_variable['sort_by']),
                'columns' => $escape->toSql($cron_variable['columns']),
                'order' => $escape->toSql($cron_variable['order']),
            ));
        }

        $template->load('cron', 'footer.tpl.php');

        $content = ob_get_clean();
        echo $content;

        // $bool =  wp_mail('viraj@prateeksha.com', 'test', $content);
        //die($bool);
        //wp_mail('viraj@prateeksha.com', 'test', $content);
        //wp_mail('sumeet@prateeksha.com', 'test', $content);

        exit();
    }

    /**
     * Method to show the box for Leads
     *
     * @uses WP_Query, $query->have_posts(), get_the_ID, get_the_title, get_the_date
     * @uses radient\getPostMeta
     *
     * @return string HTML data of the box
     */
    public static function renderCron($args)
    {

        $default = (array(
            'post_type' => 'crmleads',
            'post_status' => 'publish',
            'order' => 'ASC',
            'orderby' => 'lead_date',
            'columns' => array(),
        ));
        $args = wp_parse_args($args, $default);
        extract($args);

        $app = Sharanconsultation_Init();
        $model = $app->getModel($post_type);
        $model->setPostsPerPage(20)->setOrder($orderby, $order);
        //->setDebug(0);
        $rows = $model->getList();
        echo "<h4>" . ucfirst($post_type) . "</h4>";
        $column['titles'] = array();
        $column['data'] = array();
        $column['width'] = array();
        $column['callback'] = array();

        $cron_variables = $app->settings->getFromArray('cron_variables', $post_type);

        if ($cron_variables) {

            foreach ($cron_variables as $cron_variable) {

                $column['titles'][$cron_variable['id']] = $cron_variable['name'];

                $column['data'][$cron_variable['id']] = $cron_variable['datatype'];

                $column['width'][$cron_variable['id']] = "400px";
                if (isset($cron_variable['width'])) {
                    $column['width'][$cron_variable['id']] = $cron_variable['width'];
                }

                $column['callback'][$cron_variable['id']] = "";
                if (isset($cron_variable['callback'])) {
                    $column['callback'][$cron_variable['id']] = $cron_variable['callback'];
                }
            }
        }

        $table_width = "100%";

        $template = Sharanconsultation_Init()->template;
        $template->clear();
        $template->rows = $rows;
        $template->columns = $columns;
        $template->column = $column;
        $template->load('cron', 'rows.tpl.php', false);

        wp_reset_query();

    }

}
