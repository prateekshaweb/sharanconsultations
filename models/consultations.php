<?php

/**
 * RDSWORKORDERS - Ticketing  Management
 *
 * @category Tasks
 * @package RDSWORKORDERS
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace sharanconsultation;

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class RDSWORKORDERS
 * http://umesh/falcon/wordpress/?page_id=26&controller=activities
 *
 * @category Tasks
 * @package RDSWORKORDERS
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

class Sharanconsultation_Models_Consultations extends \radient\Radient_Classes_Models
{
	
	/**
	 * Main Post Type
	 *
	 * @var string
	 */
	var $post_type = 'consultations';
		
}
