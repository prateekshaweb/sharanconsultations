<?php

namespace sharanconsultation;


// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}


/**
 * Class MetaBox_Eventmedications
 *
 * @category events
 * @package Prateeksha_template
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class Sharanconsultation_MetaBox_Consultations_Medication extends \radient\Radient_Classes_Metabox
{

    /**
     * Method to register the box
     *
     * @param array $args
     *
     * @return void
     */
    public static function init()
    {
        $defaults = array(
            'posttype' => array(
                'consultations',
            ),
           'namespace'=>__NAMESPACE__,
            'key'=>'medications',
            'classname' => __CLASS__,
            'filename' => __FILE__,
            'function' => 'show',
            'id' => 'medications',
            'label' => __('Current Medication'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * Block comment
     *
     * @param object $post
     * 
     * @return void
     */
    public static function show($post)
    {
        // Helper objects
          $instance = \radient\Init::get_instance(__NAMESPACE__);
        $request = $instance->request;

        /**
         * Get saved medications.
         * Because it is an array, we have used if statement
         */
        $saved_medications = get_post_meta($post->ID, 'medications', true);
        if ($saved_medications) {
            $medications = $request->post('medications', $saved_medications, 'array');
        } else {
            $medications = $request->post('medications', array(), 'array');
        }

        // Remove all empty notes
        foreach ($medications as $key => $facility) {
            if (empty($facility)) {
                unset($medications[$key]);
            }
        }

        ?>
<script>
        num = <?php echo count($medications)+1; ?>;

        addFacility = function() {
            str = '<tr class="medications" style="background-color: lavender">' + '<td  width="5%" align="center">'
            + num
            + '</td>'
            + '<td width="35%"  align="center" valign="top"><input name="medications['
            + num
            + '][taken]" type="text">'
            +'</td>'
            +'<td width="10%"  align="center" valign="top">'
            + '<input type="text" class="for_what" name="medications['
            + num
            + '][for_what]" />'
            + '</td>'    
            +'<td width="10%"  align="center" valign="top">'
            + '<input type="text" class="dosage" name="medications['
            + num
            + '][dosage]" />'
            + '</td>'     
            +'<td width="10%"  align="center" valign="top">'
            + '<input type="text" class="time" name="medications['
            + num
            + '][time]" />'
            + '</td>' 
            +'<td width="10%"  align="center" valign="top">'
            + '<input type="text" class="period" name="medications['
            + num
            + '][period]" />'
            + '</td>' 
            +'<td width="10%"  align="center" valign="top">'
            + '<input type="text" class="problems" name="medications['
            + num
            + '][problems]" />'
            + '</td>'    
            +'<td width="10%"  valign="top"><input type="button" value="Delete" class="button delete" onClick="deleteFacility(this);" /></td>'
            + '</tr>';
            jQuery('#articleList3 #total-rows').before(str);
            num++;
        }

        deleteFacility = function(obj) {
            jQuery(obj).parents('tr').remove();

        }

        </script>

<div width="100%" style="overflow: scroll; overflow-x: scroll; overflow-y: hidden; ">
<p>
List all the medications and supplements being taken in detail. Please list every individual dose as a separate medication. Please list every supplement and topical ointments or applications in details.
</p>
        <input type="button" id="add" value="Add" class="button button-primary button-large" onClick="addFacility();" style="margin-bottom: 10px"  />
     <table cellpadding="10" cellspacing="0" class="table table-stripped clearfix" id="articleList3" width="100%">
                <tr style="background-color: #e6e6e6">
                <th>
                Sr.no
                </th>
                        <th width="5%"  align="center" valign="top"><?php echo __('Medication taken', 'sharantemplate'); ?></th cellpadding>
                        <th width="35%" align="center" valign="top"><?php echo __('For what'); ?></th>
                        <th width="35%" align="center" valign="top"><?php echo __('Dosage (mg, ml, etc.)'); ?></th>                     <th width="35%" align="center" valign="top"><?php echo __('At what time'); ?></th>  
                        <th width="35%" align="center" valign="top"><?php echo __('Since approximately how long'); ?></th>  
                        <th width="35%" align="center" valign="top"><?php echo __('Any problems with this medication?'); ?></th>  
                        <th width="5%"></th>
                        <th></th>
                </tr>
                <?php

        if ($medications) {
            $k = 1;
            $i = 0;
            foreach ($medications as $facility) {
                ?>
                 <tr class="medications" style="background-color: lavender">
                        <td align="center" valign="top"><?php echo $k; ?></td>
                        <td align="center" valign="top"><input type="text" class="name" name="medications[<?php echo $i; ?>][taken]" value="<?php echo $facility['taken']; ?>" size="15" />
                        </td>
                      
<td align="center"><input type ="text" class="for_what" name="medications[<?php echo $i; ?>][for_what]" style="width: 100%;" value="<?php echo $facility['for_what']; ?>" /></td>

<td align="center"><input type ="text" class="dosage" name="medications[<?php echo $i; ?>][dosage]" style="width: 100%;" value="<?php echo $facility['dosage']; ?>" /></td>

<td align="center"><input type ="text" class="time" name="medications[<?php echo $i; ?>][time]" style="width: 100%;" value="<?php echo $facility['time']; ?>" /></td>

<td align="center"><input type ="text" class="period" name="medications[<?php echo $i; ?>][period]" style="width: 100%;" value="<?php echo $facility['period']; ?>" /></td>

<td align="center"><input type ="text" class="problems" name="medications[<?php echo $i; ?>][problems]" style="width: 100%;" value="<?php echo $facility['problems']; ?>" /></td>
                  <td align="center" valign="top"><input type="button" value="Delete" class="button-danger button-small" onClick="deleteFacility(this);" style="background-color: #e06464;" /></td>
                </tr>
                </tr>
                
                        <?php
$k++;
                $i++;
            }
        }

        ?>

                <tr id="total-rows">
                        <td></td>
                        <td></td>
                        <td></td>

                </tr>


        </table>

</div>
<input type="hidden" name="facility_meta_noncename" id="facility_meta_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
<?php
return;
    }

/**
 * Method to save the meta box.
 *
 * @param integer $post_id
 *            Post ID
 *
 * @return number
 */
    public static function save($post_id)
    {
        $post = get_post($post_id);

        // Verify Nonce
        if (!wp_verify_nonce(@$_POST['facility_meta_noncename'], plugin_basename(__FILE__))) {
            return $post->ID;
        }

        // Is the user allowed to edit the post or page?
        if (!current_user_can('edit_post', $post->ID)) {
            return $post->ID;
        }

        // Request Handle
        $instance = \radient\Init::get_instance(__NAMESPACE__);
        $request = $instance->request;

        // Now Save
        $postmeta = array();

        $medications = $request->post('medications', array(), 'array', 'array');

        // Iterate and check all the data
        foreach ($medications as $key => $facility) {

               $defaults = array(
                'taken' => '',
                'for_what' => '',
                'dosage' => '',
                'time' => '',
                'problems' => '',
               
            );
            $facility = wp_parse_args($facility, $defaults);
            $facility['taken'] = \radient\Radient_Helpers_Filter::clean($facility['taken'], 'string');
            $facility['for_what'] = \radient\Radient_Helpers_Filter::clean($facility['for_what'], 'string');
            $facility['dosage'] = \radient\Radient_Helpers_Filter::clean($facility['dosage'], 'string');
            $facility['time'] = \radient\Radient_Helpers_Filter::clean($facility['time'], 'string');  
            $facility['period'] = \radient\Radient_Helpers_Filter::clean($facility['period'], 'string'); 
            $facility['problems'] = \radient\Radient_Helpers_Filter::clean($facility['problems'], 'string');
            $medications[$key] = $facility;

        }

        $postmeta['medications'] = $medications;

        // Save medications
        $instance->postmeta->save($post_id, $postmeta);

        return $post->ID;
    }

}
