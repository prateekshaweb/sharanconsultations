<?php

/**
 * sharanconsultation - Project Management
 *
 * @category Tasks
 * @package sharanconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace sharanconsultation;
use \radient\Radient_Classes_Postmeta;
use \radient\Radient_Helpers_Filter;
use \radient\Radient_Helpers_Controls;
use \radient\Radient_Classes_Metabox;
use \radient\Radient_Init;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class sharanConsultation
 *
 */
class Sharanconsultation_MetaBox_Consultations_Patientdata extends Radient_Classes_Metabox
{



    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function init()
    {
        $defaults = array(
            'posttype' => array(
                'consultations',
            ),
            'namespace'=>__NAMESPACE__,
            'key'=>'patientdata',
            'classname' => __CLASS__,
            'filename' => __FILE__,
            'function' => 'show',
            'id' => 'patientdata',
            'label' => __('Patient Data'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
              'variables'=>array(
           "consultation_date"=> array(
        "id"=> "consultation_date",
        "name"=> "Consultation Date",
        "type"=> "date",
        "class"=> "fullwidth-text",
        "col"=> 2
            ),
      "patient_fname"=> array(
        "id"=> "patient_fname",
        "name"=> "First Name",
        "type"=> "text",
        "class" => "fullwidth-text",
      "col" => 1,
        "line_break" => false,
       ),
      "patient_lname"=> array(
        "id"=> "patient_lname",
        "name"=> "Last Name",
        "type"=> "text",
       "class" => "fullwidth-text",
    
         ),
      "patient_gender"=> array(
        "id"=> "patient_gender",
        "name"=> "Gender",
        "class"=> "fullwidth-text",
        "type"=> "text",
        "choices"=> array(
            "Male"=>"Male",
            "Female"=>"Female"
        ),
        "col"=> 1
         ),
      "patient_address"=> array(
        "id"=> "patient_address",
        "name"=> "Address",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
        ),
      "patient_phone"=> array(
        "id"=> "patient_phone",
        "name"=> "Phone No",
        "class"=> "fullwidth-text",
        "type"=> "text",
        "col"=> 1
         ),
      "patient_email"=> array(
        "id"=> "patient_email",
        "name"=> "patient_dob",
        "class"=> "fullwidth-text",
        "type"=> "text",
        "col"=> 1
         ),
      "patient_dob"=> array(
        "id"=> "patient_dob",
        "name"=> "Date of Birth",
        "class"=> "fullwidth-text",
        "type"=> "date",
        "col"=> 1
         ),
      "patient_age"=> array(
        "id"=> "patient_age",
        "name"=> "Age",
        "class"=> "fullwidth-text",
        "type"=> "text",
        "col"=> 1
         ),
      "patient_reffered_by"=> array(
        "id"=> "patient_reffered_by",
        "name"=> "Reffered By",
        "class"=> "fullwidth-text",
        "type"=> "text",
        "col"=> 1
         ),
    
              )
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

   

      /**
     * Block comment
     *
     * @param object $post
     *
     * @return void
     */
    public static function show($post)
    {
        // Helper objects
        $instance = \radient\Init::get_instance(__NAMESPACE__);
        $request = $instance->request;

        /**
         * Get saved situations.
         * Because it is an array, we have used if statement
         */
        $saved_situations = get_post_meta($post->ID, 'situations', true);
        if ($saved_situations) {
            $situations = $request->post('situations', $saved_situations, 'array');
        } else {
            $situations = $request->post('situations', array(), 'array');
        }

        // Remove all empty notes
        foreach ($situations as $key => $situation) {
            if (empty($situation)) {
                unset($situations[$key]);
            }
        }

        parent::show($post);

        ?>
<script>
        num = <?php echo count($situations) + 1; ?>;

        addTimelog = function() {
            str = '<tr class="situations" style="background-color: lavender">' + '<td  width="5%" align="center">'
            + num
            + '</td>'
            + '<td width="95%"  align="center" valign="top"><input name="situations['
            + num
            + '][name]" type="text" class="fullwidth-text">'
            +'</td>'
            +'<td width="10%"  valign="top"><input type="button" value="Delete" class="button delete" onClick="deleteTimelog(this);" /></td>'
            + '</tr>';
            jQuery('#articleList #total-rows').before(str);
            num++;
        }

        deleteTimelog = function(obj) {
            jQuery(obj).parents('tr').remove();

        }

        function hide(checkbox){


if(checkbox.checked == true){
        document.getElementById('tets').style.visibility='visible';
    }else{
      document.getElementById('tets').style.visibility='hidden';
   }
}

        </script>

<div ><h3>Current Situation<h3></div>
    <div width="100%" style="overflow: scroll; overflow-x: scroll; overflow-y: hidden; ">
        <input type="button" id="add" value="Add" class="button button-primary button-large" onClick="addTimelog();" style="margin-bottom: 10px"  />
         <table cellpadding="10" cellspacing="0" class="table table-stripped clearfix" id="articleList" width="100%">
                <tr style="background-color: #e6e6e6">
                        <th width="5%"  align="center" valign="top"><?php echo __('Sr', 'sharantemplate'); ?></th cellpadding>
                        <th width="95%" align="center" valign="top" colspan="2"><?php echo __('List your current problems and how long you have had them. Start with the oldest problem and go to the latest one.'); ?></th>
                </tr>
                <?php

        if ($situations) {
            $k = 1;
            $i = 0;
            foreach ($situations as $situation) {
                ?>
                 <tr class="situations" style="background-color: lavender">
                        <td align="center" valign="top"><?php echo $k; ?></td>
                        <td align="center" valign="top"><input type="text" class="name fullwidth-text" name="situations[<?php echo $i; ?>][name]" value="<?php echo $situation['name']; ?>" size=""   />
                        </td>
                <td align="center" valign="top"><input type="button" value="Delete" class="button-danger button-small" onClick="deleteTimelog(this);" style="background-color: #e06464;" /></td>
                </tr>
                        <?php
$k++;
                $i++;
            }
        }

        ?>

                <tr id="total-rows">
                        <td></td>
                        <td></td>
                        <td></td>

                </tr>


        </table>

</div>
<?php
return;
    }

/**
 * Method to save the meta box.
 *
 * @param integer $post_id
 *            Post ID
 *
 * @return number
 */
    public static function saveAdditional($postmeta, $post_id)
    {
        // Request object
 $instance = \radient\Init::get_instance(__NAMESPACE__);
        $request = $instance->request;
        
        $defaults = array(
                'name' => '',
                'desc' => '',
                'price' => '',
                'early_bird' => '',
                'no_people' => '',
        );
        
        $situations = $request->post('situations', array(), 'array');
        
        // Iterate and check all the data
        foreach ($situations as $key => $situation) {
			            
            $situation = wp_parse_args($situation, $defaults);

            $situation['name'] = \radient\Radient_Helpers_Filter::clean($situation['name'], 'string');
           
        }

        $postmeta['situations'] = $situations;        

        return $postmeta;
    }


}
