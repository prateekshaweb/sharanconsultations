<?php

/**
 * sharanconsultation - Project Management
 *
 * @category Tasks
 * @package sharanconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace sharanconsultation;
use \radient\Radient_Classes_Postmeta;
use \radient\Radient_Helpers_Filter;
use \radient\Radient_Helpers_Controls;
use \radient\Radient_Classes_Metabox;
use \radient\Radient_Init;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class sharanConsultation
 *
 */
class Sharanconsultation_MetaBox_Consultations_Otherinfo extends Radient_Classes_Metabox
{

  
    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function init ()
    {
        $defaults = array(
            'posttype' => array(
                'consultations',
            ),
            'namespace'=>__NAMESPACE__,
            'key'=>'otherinfo',
            'classname' => __CLASS__,
            'filename' => __FILE__,
            'function' => 'show',
            'id' => 'otherinfo',
            'label' => __('Other Details'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),

                'variables'=>array(
                   "under_any_theorapy"=> array(
        "id"=> "under_any_theorapy",
        "name"=> "Are you under any other kind of therapy or treatment besides medicines?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 2
                   ),
      "major_surgery"=> array(
        "id"=> "major_surgery",
        "name"=> "Any major past health problems or surgeries?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
  ),
      "major_allergy"=> array(
        "id"=> "major_allergy",
        "name"=> "Any major allergies or food aggravations?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
       ),
      "emergency_treatment"=> array(
        "id"=> "emergency_treatment",
        "name"=> "Any emergency treatments that you have needed in the past? Please specify.",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
       ),
      "hospitaliztion"=> array(
        "id"=> "hospitaliztion",
        "name"=> "Any hospitalization needed in the past? For what condition?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
        ),
      "exercise_regularly"=> array(
        "id"=> "exercise_regularly",
        "name"=> "Do you do any exercise regularly?",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
       ),
      "major_stress"=> array(
        "id"=> "major_stress",
        "name"=> "Do you have any major stress?",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
      ),
      "family_history"=> array(
        "id"=> "family_history",
        "name"=> "Is there a family history of any of these conditions? If yes, please describe:",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
        ),
      "daily_meal_plan"=> array(
        "id"=> "daily_meal_plan",
        "name"=> "Please list all the foods you normally take, listing all options so as to give a fair idea of your daily diet. Do not miss out the teas and coffees. Try to list everything. Details will enable your therapist to give you a diet plan as close to what you are used to and like as possible",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
       ),
      "scan_file"=> array(
        "id"=> "scan_file",
        "name"=> "Please send scans of your last laboratory tests with all the data. ",
        "class"=> "fullwidth-text",
        "type"=> "media",
        "col"=> 1
      ),
      "terms"=> array(
        "id"=> "terms",
        "name"=> "Doctors shall not be held responsible for any consequences of receiving wrong information",
        "class"=> "",
        "type"=> "checkbox",
        "choices"=>array("term"=>"I agree"),
        "col"=> 1
       )
          ),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * Method to show the task box
     * Shows all the form elements in normal form
     *
     * @return NULL
     */
    public static function show($post)
    {
        global $post;

        $values = get_post_meta($post->ID, 'items', true);
        

        // Populate the fields
        $fields = static::boxVariables();
       /* foreach ($fields as $key => &$items) {
            foreach ($items as &$field) {

                if ( isset($values[$key][$field['id_original']]) ) {
                    $field['value'] = $values[$key][$field['id_original']];
                }

                $field = self::parseField(NULL, $field);
            }
        }*/

        static::getLayout($fields);
        ?>
       


		<input type="hidden" name="<?php echo static::$key; ?>_meta_noncename" id="<?php echo static::$key; ?>_meta_noncename" value="<?php echo wp_create_nonce(plugin_basename(static::$filename)); ?>" />
		<?php
}

    /**
     * Method to get the default layout of the Variables for the backend
     * You can overwrite it if you want a different layout
     *
     * @param array $fields Array of fields
     *
     * @return void
     */
    public static function getLayout($fields)
    {
        // Fixed - number of columns to 2
        $num_of_columns = 7;
        static::$key = sanitize_key(static::$key);

        ?>
        <table id="<?php echo static::$key; ?>" width="100%" class="table-metabox" cellpadding="0px" cellspacing="0px">
           

            <tr>
               <td valign="middle" class="table-box-label"><?php echo $key + 1; ?></td>
               <td valign="middle" class="table-box-label"><?php echo $items['product_id']['input']; ?></td>
               <td valign="middle" class="table-box-label"><?php echo $items['model_no']['input']; ?></td>
               <td valign="middle" class="table-box-label"><?php echo $items['serial_no']['input']; ?></td>
               <td valign="middle" class="table-box-label" style="text-align: center"><?php echo $items['is_repair']['input']; ?></td>
               <td valign="middle" class="table-box-label" style="text-align: center"><?php echo $items['is_estimate']['input']; ?></td>
               <td valign="middle" class="table-box-label" style="text-align: center"><?php echo $items['is_warranty']['input']; ?></td>
            </tr>
    <?php

        ?>
    </table>
    <?php

    }

    /**
     * Method to save the meta box
     *
     * @return number
     */
   /* public static function save($post_id)
    {
        // Verify NOnce
        if (!wp_verify_nonce(@$_POST[static::$key . '_meta_noncename'], plugin_basename(static::$filename))) {
            return $post_id;
        }

        // Is the user allowed to edit the post or page?
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // Request object
        $request = Sharanconsultation_Init()->request;

        // PHP Validate of the variables
        $errors = static::validate();
        if ($errors) {
            foreach ($errors as $error) {
                Radient_Init::addErrorMessage(__($error, 'sharan_template'));
            }
            return false;
        }

        // Now Save
        $items = $request->post('items', array(), 'array');

        $app = Sharanconsultation_Init();
        $other_item = $app->getOptionArray('workorder_metabox_variables', 'otherinfo');

        $product_id = array();

        foreach ($items as $item) {
            
            foreach ($item as $key => &$value) {

                $field = null;

                if (isset($product_itemp[$key])) {

                    $field = $product_itemp[$key];
                    // Sanitize Method exists
                    $classname = $field['type'] . 'Sanitize';

                    if (method_exists(static::$class, $classname)) {
                        $value = static::$classname($field, $value);
                    } else if ($a = Radient_Helpers_Controls::getInput($field['type'])) {
                        $value = $a->sanitize($field, $value);
                    } else if (isset($field['sanitize'])) {
                        $value = Radient_Helpers_Filter::sanitize($value);
                    }
                }
            }

            // Add to product id, to enable it for searching
            $product_id[] = $item['product_id'];
        }

        // Save
        $postmeta = array();
        $postmeta['items'] = $items;
        $postmeta['product_id'] = $product_id;
        Radient_Classes_Postmeta::save($post_id, $postmeta);

        return $post_id;
    }*/
}
