<?php

/**
 * sharanconsultation - Project Management
 *
 * @category Tasks
 * @package sharanconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace sharanconsultation;
use \radient\Radient_Classes_Postmeta;
use \radient\Radient_Helpers_Filter;
use \radient\Radient_Helpers_Controls;
use \radient\Radient_Classes_Metabox;
use \radient\Radient_Init;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class sharanConsultation
 *
 */
class Sharanconsultation_MetaBox_Consultations_Otherinfo2 extends Radient_Classes_Metabox
{



    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function init()
    {
        $defaults = array(
            'posttype' => array(
                'consultations',
            ),
             'namespace'=>__NAMESPACE__,
            'key'=>'otherinfo',
            'classname' => __CLASS__,
            'filename' => __FILE__,
            'function' => 'show',
            'id' => 'otherinfo2',
            'label' => __('Other info'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
            'variables'=>array(
           "under_any_theorapy"=> array(
        "id"=> "under_any_theorapy",
        "name"=> "Are you under any other kind of therapy or treatment besides medicines?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 2
            ),
      "major_surgery"=> array(
        "id"=> "major_surgery",
        "name"=> "Any major past health problems or surgeries?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
       ),
      "major_allergy"=> array(
        "id"=> "major_allergy",
        "name"=> "Any major allergies or food aggravations?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
         ),
      "emergency_treatment"=> array(
        "id"=> "emergency_treatment",
        "name"=> "Any emergency treatments that you have needed in the past? Please specify.",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
         ),
      "hospitaliztion"=> array(
        "id"=> "hospitaliztion",
        "name"=> "Any hospitalization needed in the past? For what condition?",
        "type"=> "textarea",
        "class"=> "fullwidth-text",
        "col"=> 1
        ),
      "exercise_regularly"=> array(
        "id"=> "exercise_regularly",
        "name"=> "Do you do any exercise regularly?",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
         ),
      "major_stress"=> array(
        "id"=> "major_stress",
        "name"=> "Do you have any major stress?",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
         ),
      "family_history"=> array(
        "id"=> "family_history",
        "name"=> "Is there a family history of any of these conditions? If yes, please describe:",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
         ),
      "daily_meal_plan"=> array(
        "id"=> "daily_meal_plan",
        "name"=> "Please list all the foods you normally take, listing all options so as to give a fair idea of your daily diet. Do not miss out the teas and coffees. Try to list everything. Details will enable your therapist to give you a diet plan as close to what you are used to and like as possible",
        "class"=> "fullwidth-text",
        "type"=> "textarea",
        "col"=> 1
         ),
      "scan_file"=> array(
        "id"=> "scan_file",
        "name"=> "Please send scans of your last laboratory tests with all the data. ",
        "class"=> "fullwidth-text",
        "type"=> "media",
        "col"=> 1
         ),
      "terms"=> array(
        "id"=> "terms",
        "name"=> "Doctors shall not be held responsible for any consequences of receiving wrong information",
        "class"=> "",
        "type"=> "checkbox",
        "choices"=>array("term"=>"I agree"),
        "col"=> 1
         ),

      )
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    
}
