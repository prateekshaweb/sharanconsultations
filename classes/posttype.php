<?php

/**
 * sharansharanframework - Project Management.
 *
 * @category Core
 * @package sharansharanframework
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace sharanconsultation;
use \radient\Radient_Classes_PostType;
use \radient\Radient_Helpers_Controls;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * sharansharanframework - Project Management.
 *
 */
class Sharanconsultation_Classes_PostType  extends Radient_Classes_PostType 
{



    /**
     *
     */
    public static function filterAccountsId($filter_account_id, $key = 'filter_account_id')
    {
        $a = Sharanconsultation_Helpers_Controls::getInput('accounts');
        $args = array(
            'value' => $filter_account_id,
            'id' => $key,
            'show_firstline' => true,
            'firstline_text' => __('All Customers', 'sharanconsultation'),
        );
        echo $a->render($args);
    }

    /**
     *
     */
    public static function filterText($filter_text, $key, $placeholder = '')
    {
        $a = Radient_Helpers_Controls::getInput('text');
        $args = array(
            'value' => $filter_text,
            'id' => $key, 
            'placeholder' => $placeholder
        );
        echo $a->render($args);
    }



}
