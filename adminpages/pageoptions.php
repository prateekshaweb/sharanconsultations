<?php

/**
 * Settings / Options Page
 *
 * @category AdminPages
 * @subpackage Settings
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016-2017 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://www.prateeksha.com/
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Theme Options
 * This class manages all the General Options
 * http://qnimate.com/wordpress-settings-api-a-comprehensive-developers-guide/#prettyPhoto
 *
 */
class Sharanconsultation_Adminpages_Pageoptions
{

    public $_option_name = SHARANCONSULTATION_OPTIONS_NAME;
    public $_namespace = __NAMESPACE__;

    public function render()
    {
        $instance = \radient\Init::get_instance(__NAMESPACE__);
        $request = $instance->request;

        // Prepare the active tab
        $active_tab = "header-options";
        $tab = $request->get('tab', null);
        if (!is_null($tab)) {
            $active_tab = $tab;
        }
        // end for active tabs

        ?>
        <div class="wrap">
        <div id="icon-options-general" class="icon32"></div>
        <h1><?php echo __('Settings for SHARAN CONSULTATION'); ?></h1>

        <!-- wordpress provides the styling for tabs. -->
        <h2 class="nav-tab-wrapper">
                <!-- when tab buttons are clicked we jump back to the same page but with a new parameter that represents the clicked tab. accordingly we make it active -->
                <a href="?page=theme-options&tab=header-options" class="nav-tab <?php if ($active_tab == 'header-options') {echo 'nav-tab-active';}?> "><?php _e('Setting', 'sandbox');?></a>
                <a href="?page=theme-options&tab=ads-options" class="nav-tab <?php if ($active_tab == 'ads-options') {echo 'nav-tab-active';}?>"><?php _e('Advertising Options', 'sandbox');?></a>
        </h2>

        <form method="post" action="options.php">
            <?php
//add_settings_section callback is displayed here. For every new section we need to call settings_fields.
        settings_fields("header_section");

        // all the add_settings_field callbacks is displayed here
        do_settings_sections("theme-options");

        // Add the submit button to serialize the options
        submit_button();
        ?>
        </form>
    </div>
    <?php

    }

    /*WordPress Menus API.*/
    public function add_new_menu_items()
    {
        $instance = \radient\Init::get_instance(__NAMESPACE__);

        //add a new menu item. This is a top level menu item i.e., this menu item can have sub menus
        add_menu_page(
            "Plugin Options", //Required. Text in browser title bar when the page associated with this menu item is displayed.
            "Plugin Options", //Required. Text to be displayed in the menu.
            "manage_options", //Required. The required capability of users to access this menu item.
            "workorder-plugin-options", //Required. A unique identifier to identify this menu item.
            array($instance->settingspage, "render"), //Optional. This callback outputs the content of the page associated with this menu item.
            "", //Optional. The URL to the menu item icon.
            100//Optional. Position of the menu item in the menu.
        );

    }

    /**
     *  add_settings_section( $id, $title, $callback, $page );
     *
     */
    public function get_add_settings_section()
    {
        $instance = \radient\Init::get_instance($this->_namespace);
        $request = $instance->request;

        // Prepare the active tab
        $active_tab = "header-options";
        $tab = $request->get('tab', null);
        if (!is_null($tab)) {
            $active_tab = $tab;
        }
        // end for active tabs

        $return = array();

        switch ($active_tab) {

            case 'ads-options':

                $return['advertising_section'] = array(
                    'id' => 'advertising_section',
                    'title' => "Adveritising Options",
                    'callback' => array($instance->settingspage, "display_header_options_content"),
                    'page' => "theme-options",
                    'description' => 'good boy2',
                );
                break;

            case 'header-options':
            default:

                $return['header_section'] = array(
                    'id' => 'header_section',
                    'title' => "Setting",
                    'callback' => array($instance->settingspage, "display_header_options_content"),
                    'page' => "theme-options",
                    'description' =>  '',
                );
                break;
        }

        return $return;
    }

    /**
     *  add_settings_field( $id, $title, $callback, $page, $section, $args );
     */
    public function get_add_settings_field()
    {
        $instance = \radient\Init::get_instance($this->_namespace);

        $return = array();

        // Ad option sections
        $return["default_currency"] = array(
            'id' => "default_currency",
            'title' => "Set Currency",
            'callback' => array($instance->settingspage, "display_form_element"),
            'page' => "theme-options",
            'section' => "header_section",
            'args' => array(
                'type' => 'currency',
                'id' => 'default_currency',
                'name' => 'default_currency',
                'class' => 'default_currency',
                
            ),
        );

        // Header logo
        $return["header_logo"] = array(
            'id' => "header_logo",
            'title' => "Logo Url2222",
            'callback' => array($instance->settingspage, "display_form_element"),
            'page' => "theme-options",
            'section' => "header_section",
            'args' => array(
                'type' => 'text',
                'id' => 'header_logo',
                'name' => 'header_logo',
                'class' => 'header_logo',
            ),
        );

        return $return;
    }

 
}
