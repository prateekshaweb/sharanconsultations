<?php

/**
 * Class for Dashboard
 *
 * @category        Core
 * @package       sharanConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Adminpages
 *
 */
class Sharanconsultation_Adminpages_Dashboard
{
    /**
     * Default tab
     *
     * @var string Default tab
     */
    static $_namespace = __NAMESPACE__;

    /**
     * @todo Box style
     */
    static $boxes = array();

    /**
     * Method to return the boxes
     *
     * @return array();
     */
    public static function getBoxes()
    {
                // @todo put in config.json
        $return = array(
            'summarybox' => array(
                'id' => 'summarybox',
                'title' => __('Summary', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Summary', 'render'),
                'position' => 'top',
                'columns' => '2',
                'params' => array(),
            ),
            'leads-recent' => array(
                'id' => 'leads-recent',
                'title' => __('Leads recent', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Leads', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array(),
            ),
            'tasks-statusactive' => array(
                'id' => 'tasks-statusactive',
                'title' => __('Tasks Active', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Tasks', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array('status' => 'active'),
            ),
            'accounts-recent' => array(
                'id' => 'accounts-recent',
                'title' => __('Latest Accounts', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Accounts', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array(),
            ),
            'contacts-recent' => array(
                'id' => 'contacts-recent',
                'title' => __('Contacts recent', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Contacts', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array(),
            ),
            'activities-recent' => array(
                'id' => 'contacts-recent',
                'title' => __('Recent Activities', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Activities', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array(),
            ),
            'projects-recent' => array(
                'id' => 'projects-recent',
                'title' => __('Recent Projects', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Projects', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array(),
            ),
            'opportunities-recent' => array(
                'id' => 'opportunities-recent',
                'title' => __('Recent Opportunities', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Opportunities', 'render'),
                'position' => 'middle',
                'columns' => '1',
                'params' => array(),
            ),
            'links' => array(
                'id' => 'links',
                'title' => __('Quick Links', 'sharanconsultation'),
                'callback' => array('\sharanconsultation\Sharanconsultation_Adminpages_Dashboardboxes_Links', 'render'),
                'position' => 'bottom',
                'params' => array(),
            ),
        );

        return apply_filters('sharan_dashboard_boxes', $return);
    }

    /**
     * Method to render the dashboard
     * Renders all the boxes for dashboard
     * Positions Top, Left, Right, Bottom
     *
     * @return NULL
     */
    public static function render()
    {
        //$boxes = self::getBoxes();
        ?>
            <h1>Sharan Consultation <span style="font-size: 14px"><?php _e('Dashboard');?></span></h1>
            <ul class="inline">
                <li><a href=""><?php _e('Introduction');?></a></li>
                <li><a href=""><?php _e('Support');?></a></li>
                <li><a href=""><?php _e('Documentation');?></a></li>
                <li>Version <?php //echo Sharanconsultation_init::getVersion(); ?></li>
            </ul>

            <div id="sharan-dashboard" class="postbox">
                <div id="dashboard-top" style="clear:both">
                    <?php /*

foreach ($boxes as $box) {
            self::displayBox($box, 'top');
        }
        ?>
                </div>

                <div id="dashboard-middle" style="clear:both">
                    <?php
foreach ($boxes as $box) {
            self::displayBox($box, 'middle');
        }
        ?>
                </div>

                <div id="dashboard-bottom" style="clear:both">
                    <?php
foreach ($boxes as $box) {
            self::displayBox($box, 'bottom');
        }
        ?>
                </div>

            </div>
            <?php*/
    }

    /**
     * Method to display the box
     *
     * @uses call_user_func
     *
     * @param string $box  - Name of the box e.g. leads
     * @param string $position - Position - top, left, right, bottom
     *
     * @return NULL
     */
  /*  public static function displayBox($box, $position)
    {
        if ($box['position'] != $position) {
            return null;
        }

        static $lr_flag = "left";

        if (is_array($box['callback'])) {
            $content = call_user_func($box['callback'], $box['params']);
        } else {
            $content = call_user_func(array(__CLASS__, $box['callback']), $box['params']);
        }

        $width = '';
        if (isset($box['columns']) && $box['columns'] == 1) {
            $width = "width: 46%;";
            if ($lr_flag == "left") {
                $width .= "float: left;";
                $lr_flag = "right";
            } else {
                $width .= "float: right;";
                $lr_flag = "left";
            }
        } else {
            $lr_flag = "left";
        }

        ?>
            <div id="dashboard-box-<?php echo $box['id']; ?>" class="postbox dashbox dashbox-<?php echo $position; ?>" style="<?php echo $width; ?>">
                <button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text">Toggle panel: <?php echo $title; ?></span><span class="toggle-indicator" aria-hidden="true"></span></button>
                <h2 class="hndle ui-sortable-handle">
                    <span><?php _e($box['title']);?></span>
                </h2>
                <div class="inside dash-inside">
                    <div class="main">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
            <?php
    }*/

}
