<?php

/**
 * Class for Dashboard Links
 *
 * @category Adminpages
 * @package sharanConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */


namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for settings
 *
 */
class Sharanconsultation_Adminpages_Dashboardboxes_Links
{

    /**
     * Method to show the box for Leads
     *
     * @uses WP_Query, $query->have_posts(), get_the_ID, get_the_title, get_the_date
     * @uses radient\getPostMeta
     *
     * @return string HTML data of the box
     */
    public static function render($args)
    {
        $url = admin_url();
        $plug_url = plugins_url('sharanconsultation/app/assets/images/');

        ob_start();
        ?>
            <table class="highlight-boxes" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="15%">
                        <a href="<?php echo $url; ?>edit.php?post_type=crmleads"><img src="<?php echo $plug_url; ?>leads.png" alt="" height="64" width="64"><br><?php _e('Leads', 'sharanconsultation');?></a>
                    </td>
                    <td width="35%">                        
                        <a href="<?php echo $url; ?>edit-tags.php?taxonomy=crmleadsstatus"><img src="<?php echo $plug_url; ?>leads.png" alt="" height="64" width="64"><br><?php _e('Leads Status', 'sharanconsultation');?></a>
                    </td>
                    <td width="15%">
                        <a href="<?php echo $url; ?>edit.php?post_type=crmprojects"><img src="<?php echo $plug_url; ?>contact.png" alt="" height="64" width="64"><br><?php _e('Projects', 'sharanconsultation');?></a>
                    </td>
                    <td width="35%">
                        <table>
                            <tr>
                                <td>
                                    <a href="<?php echo $url; ?>edit-tags.php?taxonomy=crmprojectsstatus"><img src="<?php echo $plug_url; ?>leads.png" alt="" height="64" width="64"><br><?php _e('Projects Status', 'sharanconsultation');?></a>
                                </td>
                                <td><a href="<?php echo $url; ?>edit-tags.php?taxonomy=crmprojectsstage"><img src="<?php echo $plug_url; ?>project.png" alt="" height="64" width="64"><br><?php _e('Project Stages', 'sharanconsultation');?></a></td>
                                <td><a href="<?php echo $url; ?>edit-tags.php?taxonomy=teammember"><img src="<?php echo $plug_url; ?>teamwork.png" alt="" height="64" width="64"><br><?php _e('Team Members', 'sharanconsultation');?></a></td>
                            <tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="<?php echo $url; ?>edit.php?post_type=crmaccounts"><img src="<?php echo $plug_url; ?>account.png" alt="" height="64" width="64"><br><?php _e('Accounts', 'sharanconsultation');?></a>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <a href="<?php echo $url; ?>edit-tags.php?taxonomy=crmaccountsstatus"><img src="<?php echo $plug_url; ?>leads.png" alt="" height="64" width="64"><br><?php _e('Accounts Status', 'sharanconsultation');?></a>
                                </td>
                            <tr>
                        </table>
                    </td>

                    <td>
                    <a href="<?php echo $url; ?>edit.php?post_type=crmtasks"><img src="<?php echo $plug_url; ?>task.png" alt="" height="64" width="64"><br><?php _e('Tasks', 'sharanconsultation');?></a>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td></td>
                            <tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <a href="<?php echo $url; ?>edit.php?post_type=crmcontacts"><img src="<?php echo $plug_url; ?>contact.png" alt="" height="64" width="64"><br><?php _e('Contact', 'sharanconsultation');?></a>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <a href="<?php echo $url; ?>edit-tags.php?taxonomy=crmcontactsstatus"><img src="<?php echo $plug_url; ?>leads.png" alt="" height="64" width="64"><br><?php _e('Contacts Status', 'sharanconsultation');?></a>
                                </td>
                            <tr>
                        </table>
                    </td>

                    <td>
                    <a href="<?php echo $url; ?>edit.php?post_type=crmopportunities"><img src="<?php echo $plug_url; ?>task.png" alt="" height="64" width="64"><br><?php _e('Opportunities', 'sharanconsultation');?></a>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td><a href="<?php echo $url; ?>edit-tags.php?taxonomy=crmopportunitiesstage"><img src="<?php echo $plug_url; ?>looking.png" alt="" height="64" width="64"><br><?php _e('Opportunity Stages', 'sharanconsultation');?></a></td>
                            <tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <table>
                            <tr>
                                <td>
                                    <a href="<?php echo $url; ?>edit.php?post_type=crmtemplates"><img src="<?php echo $plug_url; ?>template.png" alt="" height="64" width="64"><br><?php _e('Template', 'sharanconsultation');?></a>
                                </td>
                                <td>
                                    <a href="<?php echo $url; ?>edit.php?post_type=crmmessages"><img src="<?php echo $plug_url; ?>message.png" alt="" height="64" width="64"><br><?php _e('Message', 'sharanconsultation');?></a>
                                </td>
                                <td>
                                <a href="<?php echo $url; ?>admin.php?page=crm-calendar"><img src="<?php echo $plug_url; ?>calendar.png" alt="" height="64" width="64"><br><?php _e('Calendar', 'sharanconsultation');?></a>
                                </td>
                                <td>
                                    <a href="<?php echo $url; ?>admin.php?page=crm-reports"><img src="<?php echo $plug_url; ?>report.png" alt="" height="64" width="64"><br><?php _e('Report', 'sharanconsultation');?></a>
                                </td>
                                <td>
                                    <a href="<?php echo $url; ?>admin.php?page=crm-settings"><img src="<?php echo $plug_url; ?>template.png" alt="" height="64" width="64"><br><?php _e('Settings', 'sharanconsultation');?></a>
                                </td>
                            <tr>
                        </table>
                    </td>
                </tr>
            </table>

        <?php

        return ob_get_clean();

    }

}