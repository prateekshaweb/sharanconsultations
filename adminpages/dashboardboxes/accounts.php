<?php

/**
 * Class for Dashboard Accounts
 *
 * @category Adminpages
 * @package sharanConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Dashboard Accounts
 *
 */
class Sharanconsultation_Adminpages_Dashboardboxes_Accounts
{

    /**
     * Method to show the box for Accounts
     *
     * @return string HTML data of the box
     */
    public static function render($args)
    {
        $default = (array(
            'post_type' => 'crmaccounts',
            'post_status' => 'publish',
            'order' => 'desc',
            'orderby' => 'account_date',
        ));
        $args = wp_parse_args($args, $default);
        extract($args);

        $model = Sharanconsultation_Init()->getModel('accounts');
        $model->setOrder($orderby, $order);
        $model->setDebug(0);
        $rows = $model->getList();

        ob_start();
        ?>
        <div class="col-md-6" style="float: left; width: 100%;">
            <table class='table-list' cellpadding='4px' cellspacing='0px'>
                <tr>
                    <th align='left'><?php _e('Name');?></th>
                    <th align='left'><?php _e('Date');?></th>
                    <th align='left'><?php _e('Customer');?></th>
                    <th align='left'><?php _e('Status');?></th>
                </tr>
                        <?php
foreach ($rows as $row) {
            ?>
                <tr>
                    <td>
                        <a href="<?php echo get_edit_post_link($row->ID); ?>">
                            <?php echo $row->post_title; ?>
                        </a>
                    </td>
                    <td>
                        <?php echo Sharanconsultation_Helpers_Common::getDateFromPostId($row->ID, 'account_date'); ?>
                    </td>
                    <td>
                        <?php $a = (int)get_post_meta($row->ID, 'contact_id', true);
                        if ( !empty($a) ) echo get_the_title($a); ?>
                    </td>
                    <td>
                        <?php echo ucfirst(get_post_meta($row->ID, 'status', true)); ?>
                    </td>
                </tr>
                <?php
}
        ?>
            </table>
        </div>
        <?php
//$model->resetQuery();

        return ob_get_clean();
    }

}