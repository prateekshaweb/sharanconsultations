<?php

/**
 * Class for Dashboard Leads
 *
 * @category Adminpages
 * @package sharanConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for settings
 *
 */
class Sharanconsultation_Adminpages_Dashboardboxes_Leads
{

    /**
     * Method to show the box for Leads
     *
     * @uses WP_Query, $query->have_posts(), get_the_ID, get_the_title, get_the_date
     * @uses radient\getPostMeta
     *
     * @return string HTML data of the box
     */
    
    public static function render($args)
    {

        $default = (array(
            'post_type' => 'crmleads',
            'post_status' => 'publish',
            'order' => 'ASC',
            'orderby' => 'lead_date',
        ));
        $args = wp_parse_args($args, $default);

        extract($args);

        $model = Sharanconsultation_Init()->getModel('leads');
        $model->setOrder($orderby, $order);
        $model->setDebug(0);
        $rows = $model->getList();

        ob_start();

        ?>
        <div class="col-md-6" style="float: left; width:100%;">
        <table class='table-list' cellpadding='4px' cellspacing='0px'>
            <tr>

                    <th align='left'><?php _e('Name');?></th>
                    <th align='left'><?php _e('Date');?></th>
                    <th align='left'><?php _e('Project');?></th>
                    <th align='left'><?php _e('Status');?></th>

            </tr>
                    <?php
foreach ($rows as $row) {
            ?>
            <tr>
                <td>
                    <a href="<?php echo get_edit_post_link($row->ID); ?>">
                        <?php echo $row->post_title; ?>
                    </a>
                </td>
                <td>
                    <?php echo Sharanconsultation_Helpers_Common::getDateFromPostId($row->ID, 'lead_date', true); ?>
                </td>
                <td>
                    <?php echo  get_post_meta($row->ID, 'company', true);
        
            ?>
                </td>
                <td>
                    <?php echo ucfirst(get_post_meta($row->ID, 'status', true)); ?>
                </td>

            </tr>
            <?php
}

        ?>
        </table>
        </div>
        <?php
//wp_reset_query();

        return ob_get_clean();

    }

}