<?php

// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit();
}

$instance = \radient\Init::get_instance(__NAMESPACE__);

?>
<div>
<?php

// Get all the variables
$vars = $this->getVariables($contact_id);
/*$args = array(
    'post_type' => 'rdsproducts',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'caller_get_posts' => 1,
    'field' => 'id',
);
//$my_query = new \WP_Query($args);
 */ ?>

	<div class="col-md-push-1 col-md-10">
		<p><a href="<?php
													echo add_query_arg(array(
														'controller' => 'activities',
														'action' => 'all',
													)); ?>" class="btnln1-green btn-shadow3 radius25 btn-block-md"><?php _e('Go Back'); ?> </a>
		</p>
			
		<div class="consult-form">
		<h4 class="heading3 text-center" style="margin: 20px 0; "><?php echo __('PATIENT DATA FORM FOR SHARAN CONSULTATIONS ', 'sharanconsultation'); ?></h4>
		<div class="row">
			<form role="form" class="form-horizontal"  method="post" name="front_end" action="" >
				<div class="clearfix">
					<div class="col-md-6">
						<label for="name" class="col-form-label">First Name</label> 
						<?php echo $vars['patient_fname']; ?>  
					</div>
					<div class="col-md-6">
						<label for="lname" class="col-form-label">Last Name</label>
						<?php echo $vars['patient_lname']; ?>  
					</div>
				</div>

				<div class="clearfix">
					<div class="col-sm-12">
						<label for="gender" class="col-form-label">Gender : </label>
						<?php  echo $vars['patient_gender']; ?>
					</div>
				</div>

				<div class="clearfix">
					<div class="col-sm-12">
						<label for="exampleInputPassword1" class="col-form-label">Address</label>
						<?php echo $vars['patient_address']; ?>
					</div>
				</div>

				<div class="clearfix">
					<div class="col-sm-6">
						<label for="exampleInputFile" class="col-form-label">Phone No</label>
						<?php echo $vars['patient_phone']; ?>         
					</div>
					<div class="col-sm-6">
						<label class="col-form-label">Email </label>
						<?php echo $vars['patient_email']; ?>
					</div>
				</div>

				<div class="clearfix">
					<div class="col-sm-6">
						<label class="col-form-label" >Date of Birth </label>
						<?php echo $vars['patient_dob'];	?>        
					</div>
					<div class="col-sm-6">
						<label class="col-form-label">Age </label>
						<?php echo $vars['patient_age']; ?>              
					</div>
				</div>
				
				<div class="clearfix">
					<div class="col-sm-12">
						<label class="col-form-label" >Reffered By </label>
						<?php echo $vars['patient_reffered_by']; ?>    
					</div>
				</div>
				
				<hr>
				<h4 class="heading3 text-center" style="margin: 20px 0; ">Current Situation</h4>
				<hr>
				<p>(List your current problems and how long you have had them. Start with the oldest problem and go to the latest one.)</p>
				
				<div class="clearfix">
					<ol>
					<?php for ($i = 0; $i < 10; $i++) { ?>
						<li class="col-md-12">
							<div class="col-md-12 no-padding">
								<?php 
							/**<label  class="col-sm-1 col-form-label"><?php echo $i+1;?></label> **/ ?>
								<?php echo $vars['situations[' . $i . '][name]'];			?>        
							</div>
						</li>
					<?php 
			} ?>
					</ol>
				</div>

				<hr>
				<h4 class="heading3 text-center" style="margin: 20px 0; ">Current Medications</h4>
				<hr>
				<p>List all the medications and supplements being taken in detail. Please list every individual dose as a separate medication. Please list every supplement and topical ointments or applications in details.</p>
		
				<div class="clearfix table-responsive">
					<table cellspacing="0" cellpadding="0" class="table">
						<thead>
							<th width="4%">No</th>
							<th width="16%">Medication taken</th>
							<th width="16%">For what</th>
							<th width="16%">Dosage (mg, ml, etc.)</th>
							<th width="16%">At what time</th>
							<th width="16%">Since approximately how long</th>
							<th width="16%">Any problems with this medication?</th>
						</thead>
						<tbody>
						<?php for ($i = 0; $i < 10; $i++) { ?>
							<tr>
								<td>
									<label  class="col-sm-1 col-form-label"><?php echo $i + 1; ?></label>
								</td>
								<td>
										<?php echo $vars['medications[' . $i . '][taken]']; ?>      
								</td>
								<td>
										<?php echo $vars['medications[' . $i . '][for_what]']; ?>     
								</td>
								<td>
									<?php echo $vars['medications[' . $i . '][dosage]']; ?>      
								</td>
								<td>
										<?php echo $vars['medications[' . $i . '][time]']; ?>     
								</td>
								<td>
										<?php echo $vars['medications[' . $i . '][period]']; ?>      
								</td>
								<td>
										<?php echo $vars['medications[' . $i . '][problems]']; ?>   
								</td>
								</tr>
							<?php 
					} ?> 
						</tbody>
					</table>
				</div>

				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Are you under any other kind of therapy or treatment besides medicines? </label>
									<?php echo $vars['under_any_theorapy']; ?>        
					</div>
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label">Any major past health problems or surgeries? </label>
								<?php echo $vars['major_surgery']; ?>        
					</div>
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Any major allergies or food aggravations?  </label>
								<?php echo $vars['major_allergy']; ?>      
					</div>
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Any emergency treatments that you have needed in the past? Please specify. </label>
									<?php echo $vars['emergency_treatment']; ?>      
					</div>    
				</div>

				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Any hospitalization needed in the past? For what condition? </label>
									<?php echo $vars['hospitaliztion']; ?>      
					</div>
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Do you do any exercise regularly? </label>
									<?php echo $vars['exercise_regularly']; ?>         
					</div>
				</div>

				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Do you have any major stress? </label>
								<?php echo $vars['major_stress']; ?>         
					</div> 
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Is there a family history of any of these conditions? If yes, please describe: </label>
									<?php echo $vars['family_history']; ?>      
					</div>     
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Please list all the foods you normally take, listing all options so as to give a fair idea of your daily diet. Do not miss out the teas and coffees. Try to list everything. Details will enable your therapist to give you a diet plan as close to what you are used to and like as possible</label>
								<?php echo $vars['daily_meal_plan']; ?>            
					</div>
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label for="gender"  class="col-form-label">Please send scans of your last laboratory tests with all the data</label>
									<?php echo $vars['scan_file']; ?>      
					</div>  
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<label class="col-form-label" >Doctors shall not be held responsible for any consequences of receiving wrong information.</label>
					</div>
					<div class="col-md-12">
								<?php echo $vars['terms']; ?>      
					</div>        
				</div>
				
				<div class="clearfix">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Submit</button>
					</div>
				</div>
				<input type="hidden" name="controller" value="addconsultations" />         
				<input type="hidden" name="id" value="" />
				<input type="hidden" name="action" value="confirm" id="action" /> 
				<input type="hidden" name="subaction" value="add" />
			</form>
		</div>
	</div>
</div>



