<?php

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

?>

<p><a href="<?php
            echo add_query_arg(array(
                'controller' => 'activities',
                'action' => 'all',
            )); ?>" class="btnln1-green btn-shadow3 radius25 btn-block-md"><?php _e('Go Back'); ?> </a></p>

<h3 class="heading3 text-center"><?php echo __('PATIENT DATA FORM FOR SHARAN CONSULTATIONS ', 'sharanconsultation'); ?></h3>
<div>
<?php
/*$args = array(
    'post_type' => 'rdsproducts',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'caller_get_posts' => 1,
    'field' => 'id',
);
//$my_query = new \WP_Query($args);
 */ ?>

<div class="container">
	<div class="col-md-push-2 col-md-10">
	<div class="row">
		<form role="form" class="form-horizontal"  method="post" name="front_end" action="" >
			<div class="form-group">
				<div class="col-md-6"><label for="name" class="col-form-label">First Name</label></div>
				<div class="col-sm-4">  
					<?php $a = \radient\Radient_Helpers_Controls::getInput('text');
    $args = array(
        'class' => 'form-control',
        'name' => 'First Name',
        'id' => 'patient_fname',
        'show_desc' => true,
        'position_desc' => 1, // 1 = Side, 2 = Below,
        'attributes' => array(),
        'placeholder' => 'First Name',
        'disabled' => false,
        'default' > '',
    );
    echo $a->render($args);
    ?>  
				</div>
       
     
            <label for="lname" class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-4">   <?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('text');
                                    $args = array(
                                        'class' => 'form-control ',
                                        'name' => 'Last Name',
                                        'id' => 'patient_lname',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'attributes' => array(),
                                        'placeholder' => '',
                                        'disabled' => false,
                                        'default' > '',
                                    );
                                    echo $a->render($args);
                                    ?>  </div>
        </div>

        <div class="form-group row">
            <label for="gender"  class="col-sm-2 col-form-label">Gender   :</label>
            <div class="col-sm-4">   <?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('radio');
                                    $args = array(
                                        'class' => ' ',
                                        'name' => '',
                                        'id' => 'patient_gender',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'choices' => array(
                                            "Male" => "Male",
                                            "Female" => "Female"
                                        ),
                                        'placeholder' => '',
                                        'disabled' => false,
                                        'default' > '',
                                    );
                                    echo $a->render($args);
                                    ?>  
    </div>
        </div>

   
          <div class="form-group row">
            <label for="exampleInputPassword1" class="col-sm-2 col-form-label">Address</label>
            <div class="col-sm-10">   <?php
                                        $a = \radient\Radient_Helpers_Controls::getInput('textarea');
                                        $args = array(
                                            'class' => 'form-control',
                                            'name' => 'Address',
                                            'id' => 'patient_address',
                                            'show_desc' => true,
                                            'position_desc' => 1, // 1 = Side, 2 = Below,
                                            'attributes' => array(),
                                            'placeholder' => '',
                                            'disabled' => false,
                                            'default' > '',
                                        );
                                        echo $a->render($args);
                                        ?>  </div>
        </div>
   
        <div class="form-group row">
            <label for="exampleInputFile" class="col-sm-2 col-form-label">Phone No</label>
            <div class="col-sm-4">   <?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('text');
                                    $args = array(
                                        'class' => 'form-control',
                                        'name' => 'Phone No',
                                        'id' => 'patient_phone',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'attributes' => array(),
                                        'placeholder' => '',
                                        'disabled' => false,
                                        'default' > '',
                                    );
                                    echo $a->render($args);
                                    ?>         
        </div>
       
            <label  class="col-sm-2 col-form-label">     Email </label>
            <div class="col-sm-4">    <?php
                                        $a = \radient\Radient_Helpers_Controls::getInput('text');
                                        $args = array(
                                            'class' => 'form-control',
                                            'name' => 'Email',
                                            'id' => 'patient_email',
                                            'show_desc' => true,
                                            'position_desc' => 1, // 1 = Side, 2 = Below,
                                            'attributes' => array(),
                                            'placeholder' => '',
                                            'disabled' => false,
                                            'default' > '',
                                        );
                                        echo $a->render($args);
                                        ?>  </div>
        </div>

      <div class="form-group row">
            <label class="col-sm-2 col-form-label" >Date of Birth </label>
            <div class="col-sm-4"><?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('date');
                                    $args = array(
                                        'class' => 'form-control',
                                        'name' => 'Date of Birth ',
                                        'id' => 'patient_dob',
                                        'value' => '',
                                        'desc' => '',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'attributes' => array(),
                                    );
                                    echo $a->render($args);
                                    ?>        
        </div>
       
            <label class="col-sm-2 col-form-label">Age </label>
            <div class="col-sm-4">  <?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('number');
                                    $args = array(
                                        'class' => 'form-control',
                                        'name' => 'Age ',
                                        'id' => 'patient_age',
                                        'value' => '',
                                        'desc' => '',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'attributes' => array(),
                                    );
                                    echo $a->render($args);
                                    ?>        
        </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" >Reffered By </label>
            <div class="col-sm-4">  <?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('text');
                                    $args = array(
                                        'class' => 'form-control',
                                        'name' => 'Reffered By ',
                                        'id' => 'patient_reffered_by',
                                        'value' => '',
                                        'desc' => '',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'attributes' => array(),
                                    );
                                    echo $a->render($args);
                                    ?>        
    </div>
        </div>
        <h3> Current Situation </h3>
        <p>(List your current problems and how long you have had them. Start with the oldest problem and go to the latest one.)</p>

<?php for ($i = 0; $i < 10; $i++) {
    ?>
     
     <div class="form-group row">
           <label  class="col-sm-1 col-form-label"><?php echo $i + 1; ?></label>
           <div class="col-sm-11">        <?php
                                            $a = \radient\Radient_Helpers_Controls::getInput('text');

                                            $args = array(
                                                'class' => 'form-control',
                                                'name' => $i,
                                                'id' => 'situations[' . $i . '][name]',
                                                'value' => '',
                                                'desc' => '',
                                                'show_desc' => true,
                                                'position_desc' => 1, // 1 = Side, 2 = Below,
                                                'attributes' => array(),
                                            );
                                            echo $a->render($args);
                                            ?>        
        </div>
        </div>
<?php 
} ?> 
     
        
<h3>Current Medications</h3>
        <p>List all the medications and supplements being taken in detail. Please list every individual dose as a separate medication. Please list every supplement and topical ointments or applications in details.</p>
    
        <div class="form-group row">
           <label  class="col-sm-1 col-form-label">No</label>
       
           <label  class="col-sm-2 col-form-label">Medication taken</label>
        
           <label  class="col-sm-2 col-form-label">For what</label>
        
           <label  class="col-sm-1 col-form-label">Dosage (mg, ml, etc.)</label>
        
           <label  class="col-sm-2 col-form-label">At what time</label>
       
           <label  class="col-sm-2 col-form-label">Since approximately how long</label>
        
           <label  class="col-sm-2 col-form-label">Any problems with this medication?</label>
        </div>
<?php for ($i = 0; $i < 10; $i++) {
    ?>
        <div class="form-group row">
           <label  class="col-sm-1 col-form-label"><?php echo $i + 1; ?></label>
       
        <div  class="col-sm-2">
              <?php
                $a = \radient\Radient_Helpers_Controls::getInput('text');

                $args = array(
                    'class' => 'form-control',
                    'name' => $i,
                    'id' => 'medications[' . $i . '][taken]',
                    'value' => '',
                    'desc' => '',
                    'show_desc' => true,
                    'position_desc' => 1, // 1 = Side, 2 = Below,
                    'attributes' => array(),
                );
                echo $a->render($args);
                ?>        
        </div>
        <div class="col-sm-2 ">
              <?php
                $a = \radient\Radient_Helpers_Controls::getInput('text');

                $args = array(
                    'class' => 'form-control',
                    'name' => $i,
                    'id' => 'medications[' . $i . '][for_what]',
                    'value' => '',
                    'desc' => '',
                    'show_desc' => true,
                    'position_desc' => 1, // 1 = Side, 2 = Below,
                    'attributes' => array(),
                );
                echo $a->render($args);
                ?>        
        </div>
        <div class="col-sm-1">
              <?php
                $a = \radient\Radient_Helpers_Controls::getInput('text');

                $args = array(
                    'class' => 'form-control',
                    'name' => $i,
                    'id' => 'medications[' . $i . '][dosage]',
                    'value' => '',
                    'desc' => '',
                    'show_desc' => true,
                    'position_desc' => 1, // 1 = Side, 2 = Below,
                    'attributes' => array(),
                );
                echo $a->render($args);
                ?>        
        </div>
        <div class="col-sm-2 ">
              <?php
                $a = \radient\Radient_Helpers_Controls::getInput('text');

                $args = array(
                    'class' => 'form-control',
                    'name' => $i,
                    'id' => 'medications[' . $i . '][time]',
                    'value' => '',
                    'desc' => '',
                    'show_desc' => true,
                    'position_desc' => 1, // 1 = Side, 2 = Below,
                    'attributes' => array(),
                );
                echo $a->render($args);
                ?>        
        </div>
        <div class="col-sm-2">
              <?php
                $a = \radient\Radient_Helpers_Controls::getInput('text');

                $args = array(
                    'class' => 'form-control',
                    'name' => $i,
                    'id' => 'medications[' . $i . '][period]',
                    'value' => '',
                    'desc' => '',
                    'show_desc' => true,
                    'position_desc' => 1, // 1 = Side, 2 = Below,
                    'attributes' => array(),
                );
                echo $a->render($args);
                ?>        
        </div>
        <div  class="col-sm-2">
              <?php
                $a = \radient\Radient_Helpers_Controls::getInput('text');

                $args = array(
                    'class' => 'form-control',
                    'name' => $i,
                    'id' => 'medications[' . $i . '][problems]',
                    'value' => '',
                    'desc' => '',
                    'show_desc' => true,
                    'position_desc' => 1, // 1 = Side, 2 = Below,
                    'attributes' => array(),
                );
                echo $a->render($args);
                ?>        
        </div>
        </div>
<?php 
} ?> 

 <div class="form-group row">
            <label class="col-sm-12 col-form-label" >
     Are you under any other kind of therapy or treatment besides medicines? </label>
       
        <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'under_any_theorapy',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>        
    </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Any major past health problems or surgeries? </label>
   
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'major_surgery',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>        
    </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Any major allergies or food aggravations?  </label>
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'major_allergy',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>        
        </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Any emergency treatments that you have needed in the past? Please specify. </label>
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'emergency_treatment',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>    </div>    
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Any hospitalization needed in the past? For what condition? </label>
    
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'hospitaliztion',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Do you do any exercise regularly? </label>
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'exercise_regularly',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>        
        </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >
       Do you have any major stress? </label>

        <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'major_stress',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>       
    </div> 
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Is there a family history of any of these conditions? If yes, please describe: </label>
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'family_history',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>   </div>     
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Please list all the foods you normally take, listing all options so as to give a fair idea of your daily diet. Do not miss out the teas and coffees. Try to list everything. Details will enable your therapist to give you a diet plan as close to what you are used to and like as possible</label>
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('textarea');

        $args = array(
            'class' => 'form-control',
            'name' => '',
            'id' => 'daily_meal_plan',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        echo $a->render($args);
        ?>        
    </div>
        </div>
        <div class="form-group row">
            <label for="gender"  class="col-sm-3 col-form-label">Please send scans of your last laboratory tests with all the data</label>
            <div class="col-sm-9">   <?php
                                    $a = \radient\Radient_Helpers_Controls::getInput('media');

                                    $args = array(
                                        'class' => 'form-control',
                                        'name' => '',
                                        'id' => 'scan_file',
                                        'value' => '',
                                        'desc' => '',
                                        'show_desc' => true,
                                        'position_desc' => 1, // 1 = Side, 2 = Below,
                                        'attributes' => array(),
                                    );
                                    echo $a->render($args);
                                    ?>         </div>  
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-form-label" >Doctors shall not be held responsible for any consequences of receiving wrong information.</label>
   
            <div class="col-sm-12">
        <?php
        $a = \radient\Radient_Helpers_Controls::getInput('checkbox');

        $args = array(
            'class' => '',
            'name' => '',
            'id' => 'terms',
            'attributes' => array(),
            'choices' => array("term" => "I Agree"),
            'show_label' => true,
        );
        echo $a->render($args);
        ?>        
        </div>        
        </div>
        <div class="form-group row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
        </div>
        <input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />         
        <input type="hidden" name="action" value="save" id="action" /> 
        <input type="hidden" name="subaction" value="add" />
    </form>

	</div>
	</div>
</div>



