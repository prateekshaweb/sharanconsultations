<?php

/**
 * rdsworkorders - Edit Screen
 *
 * @category Projects
 *
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @link http://www.prateeksha.com/
 *
 */
namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}
// Request
$request = \radient\Init::get_instance(__NAMESPACE__)->request;

$vars = $this->formvariables;

$current_user = wp_get_current_user();
?>
<h3>Confirmation screen</h3>
<form method="POST" class="clearfix" name="editform" id="editform">
      <?php

        ?>

<div class="form-group">

           <div class ="col-md-12"> Date : 
                <?php echo date("d-m-Y"); ?>
</div>
<div class ="col-md-6">
<strong> Name</strong>
    </div>
    
<div class ="col-md-6">
    <input type="hidden" name="patient_fname" value="<?php echo $request->post('patient_fname', '', 'string'); ?>" />
    <input type="hidden" name="patient_lname" value="<?php echo $request->post('patient_lname', '', 'string'); ?>" />
    <?php echo $request->post('patient_fname', '', 'string'); ?> 
	 <?php echo $request->post('patient_lname', '', 'string'); ?>
</div>  

<div class ="col-md-6"><strong>
 <?php echo __('Gender'); ?> </strong>
    </div>
    
<div class ="col-md-6">
     <input type="hidden" name="patient_gender" value="<?php echo $request->post('patient_gender', '', 'string'); ?>" />
<?php echo $request->post('patient_gender', '', 'string'); ?>
</div>           

<div class ="col-md-6">
 <strong><?php echo __('Address'); ?> </strong>
    </div>
    
<div class ="col-md-6">
     <input type="hidden" name="patient_address" value="<?php echo $request->post('patient_address', '', 'string'); ?>" />
	 <?php echo $request->post('patient_address', '', 'string'); ?>
</div>         

    <div class ="col-md-6">
<strong> <?php echo __('Phone No'); ?></strong>
 <input type="hidden" name="patient_phone" value="<?php echo $request->post('patient_phone', '', 'string'); ?>" />
         <?php echo $request->post('patient_phone', '', 'string'); ?></div> 
         
<div class ="col-md-6">
<strong><?php echo __('Email'); ?> </strong> <input type="hidden" name="patient_email" value="<?php echo $request->post('patient_email', '', 'string'); ?>" />	 <?php echo $request->post('patient_email', '', 'string'); ?>
</div>   

     <div class ="col-md-6">
 <strong><?php echo __('Date of Birth'); ?> </strong>
     <input type="hidden" name="patient_dob" value="<?php echo $request->post('patient_dob', '', 'string'); ?>" />
    <?php echo $request->post('patient_dob', '', 'string'); ?>    
     </div>	
    <div class ="col-md-6">
         <strong><?php echo __('Age'); ?> </strong>
     <input type="hidden" name="patient_age" value="<?php echo $request->post('patient_age', '', 'string'); ?>" />
    <?php echo $request->post('patient_age', '', 'string'); ?>   
        </div>

<div class ="col-md-6">
 <strong><?php echo __('Reffered By'); ?> </strong>
    </div>
    
<div class ="col-md-6">
     <input type="hidden" name="patient_reffered_by" value="<?php echo $request->post('patient_reffered_by', '', 'string'); ?>" />
	 <?php echo $request->post('patient_reffered_by', '', 'string'); ?>
</div>    
            <div class="tablecontainer clearfix" style="padding: 20px">
        <table width="100%" id="articleList3" class="table-striped" >
            <tr>
                <th width="2%">Sr</th>
              
                <th width="15%">Current Situation</th>
            </tr>

 <?php
$rows = $request->post('situations', array(), 'array');
$c = 0;
$key = 0;

if ($rows) {
    foreach ($rows as $row) {
        echo ('<tr>');
        $c++;


        echo ('<td>' . $c . '</td>');
        echo ('<td>' . $row['name'] . '</td>');


        ?>
    <input type="hidden" name="situations[<?php echo $key; ?>][name]" value="<?php echo $row['name']; ?>" />
        <?php
        $key++;


        ?>

    <?php
    echo ('</tr>');
}
} ?>    </table>
        </div>
        <div class="clearfix"></div>
           <div class="tablecontainer clearfix" style="padding: 20px">
        <table width="100%" id="articleList3" class="table-striped" >
            <tr>
                <th width="2%">Sr</th>
              <th width="15%">Medication taken</th>
              <th width="15%"> 	For what</th>
              <th width="15%">Dosage (mg, ml, etc.)</th>
              <th width="15%"> 	At what time</th>
              <th width="15%">Since approximately how long</th>
                <th width="15%">Any problems with this medication?</th>
            </tr>

 <?php
$rows = $request->post('medications', array(), 'array');
$c = 0;
$key = 0;

if ($rows) {
    foreach ($rows as $row) {
        echo ('<tr>');
        $c++;


        echo ('<td>' . $c . '</td>');
        echo ('<td>' . $row['taken'] . '</td>');
        echo ('<td>' . $row['for_what'] . '</td>');
        echo ('<td>' . $row['dosage'] . '</td>');
        echo ('<td>' . $row['time'] . '</td>');
        echo ('<td>' . $row['period'] . '</td>');
        echo ('<td>' . $row['problems'] . '</td>');


        ?>
    <input type="hidden" name="situations[<?php echo $key; ?>][taken]" value="<?php echo $row['taken']; ?>" />
    <input type="hidden" name="situations[<?php echo $key; ?>][for_what]" value="<?php echo $row['for_what']; ?>" />
    <input type="hidden" name="situations[<?php echo $key; ?>][dosage]" value="<?php echo $row['dosage']; ?>" />
    <input type="hidden" name="situations[<?php echo $key; ?>][time]" value="<?php echo $row['time']; ?>" />
    <input type="hidden" name="situations[<?php echo $key; ?>][period]" value="<?php echo $row['period']; ?>" />
     <input type="hidden" name="situations[<?php echo $key; ?>][problems]" value="<?php echo $row['problems']; ?>" />
        <?php
        $key++;


        ?>

    <?php
    echo ('</tr>');
}
} ?>    </table>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">

        <div  class="col-sm-12 col-md-12"> <strong>
Are you under any other kind of therapy or treatment besides medicines?</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('under_any_theorapy', '', 'string'); ?>
                <input type="hidden" name="under_any_theorapy" value="<?php echo $request->post('under_any_theorapy', '', 'string'); ?>" />

         </div>


        <div  class="col-sm-12 col-md-12"> <strong>Any major past health problems or surgeries? </strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('major_surgery', '', 'string'); ?>
                <input type="hidden" name="major_surgery" value="<?php echo $request->post('major_surgery', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Any major allergies or food aggravations?  </strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('major_allergy', '', 'string'); ?>
                <input type="hidden" name="major_allergy" value="<?php echo $request->post('major_allergy', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Any emergency treatments that you have needed in the past? Please specify.</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('emergency_treatment', '', 'string'); ?>
                <input type="hidden" name="emergency_treatment" value="<?php echo $request->post('emergency_treatment', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Any hospitalization needed in the past? For what condition? </strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('hospitaliztion', '', 'string'); ?>
                <input type="hidden" name="hospitaliztion" value="<?php echo $request->post('hospitaliztion', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Do you do any exercise regularly?</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('exercise_regularly', '', 'string'); ?>
                <input type="hidden" name="exercise_regularly" value="<?php echo $request->post('exercise_regularly', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Do you have any major stress? </strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('major_stress', '', 'string'); ?>
                <input type="hidden" name="major_stress" value="<?php echo $request->post('major_stress', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Is there a family history of any of these conditions? If yes, please describe:</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('family_history', '', 'string'); ?>
                <input type="hidden" name="family_history" value="<?php echo $request->post('family_history', '', 'string'); ?>" />
         </div>

        <div  class="col-sm-12 col-md-12"> <strong>Please list all the foods you normally take, listing all options so as to give a fair idea of your daily diet. Do not miss out the teas and coffees. Try to list everything. Details will enable your therapist to give you a diet plan as close to what you are used to and like as possible</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('daily_meal_plan', '', 'string'); ?>
                <input type="hidden" name="daily_meal_plan" value="<?php echo $request->post('daily_meal_plan', '', 'string'); ?>" />
         </div>

           <div  class="col-sm-12 col-md-12"> <strong>Please send scans of your last laboratory tests with all the data</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('scan_file', '', 'string'); ?>
                <input type="hidden" name="scan_file" value="<?php echo $request->post('scan_file', '', 'string'); ?>" />
         </div>

           <div  class="col-sm-12 col-md-12"> <strong>Doctors shall not be held responsible for any consequences of receiving wrong information.</strong>
        </div>
         <div class="col-sm-12 col-md-12">
            <?php echo $request->post('terms', '', 'string'); ?>
                <input type="hidden" name="terms" value="<?php echo $request->post('terms', '', 'string'); ?>" />
         </div>
    </div>
        <input type="hidden" name="controller" value="addconsultations" />
        <input type="hidden" name="action" value="save" id="action" />
        <input type="hidden" name="subaction" value="confirm" />
        <?php //echo $buttons->render(); ?>
        <input type="submit" value="Confirm" />
            <?php 
            $url = add_query_arg(array(
                'controller' => 'addaworkorder',
                'action' => 'add',
            ), (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
            ?>
		<a href="<?php echo $url; ?>" class="btn ">Go Back</a>
</form>

 </div>
