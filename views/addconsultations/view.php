<?php

namespace sharanconsultation;

// Do not allow direct loading of this file.
if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	exit();
}

class Sharanconsultation_Views_Addconsultations_View extends \radient\Radient_Classes_View
{

	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	public function getVariables($contact_id)
	{
		$instance = \radient\Init::get_instance(__NAMESPACE__);

		$return = array();

		$args = array(
			'class' => 'form-control',
			'name' => 'Firstsa Name',
			'id' => 'patient_fname',
			'show_desc' => true,
			'position_desc' => 1,
			'attributes' => array(),
			'placeholder' => '',
			'default' => ''
		);
		$return['patient_fname'] = $instance->controls->get('text', $args);

		$args = array(
			'class' => 'form-control ',
			'name' => 'Last Name',
			'id' => 'patient_lname',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
			'placeholder' => '',
			'disabled' => false,
			'default' > '',
		);
		$return['patient_lname'] = $instance->controls->get('text', $args);

		$args = array(
			'class' => ' form-control ',
			'name' => '',
			'id' => 'patient_gender',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'choices' => array(
				"Male" => "Male",
				"Female" => "Female"
			),
			'placeholder' => '',
			'disabled' => false,
			'default' > '',
		);
		$return['patient_gender'] = $instance->controls->get('select', $args);

		$args = array(
			'class' => 'form-control',
			'name' => 'Address',
			'id' => 'patient_address',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
			'placeholder' => '',
			'disabled' => false,
			'default' > '',
		);
		$return['patient_address'] = $instance->controls->get('textarea', $args);

		$args = array(
			'class' => 'form-control',
			'name' => 'Phone No',
			'id' => 'patient_phone',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
			'placeholder' => '',
			'disabled' => false,
			'default' > '',
		);
		$return['patient_phone'] = $instance->controls->get('text', $args);

		$args = array(
			'class' => 'form-control',
			'name' => 'Email',
			'id' => 'patient_email',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
			'placeholder' => '',
			'disabled' => false,
			'default' > '',
		);
		$return['patient_email'] = $instance->controls->get('text', $args);


		$args = array(
			'class' => 'form-control',
			'name' => 'Date of Birth ',
			'id' => 'patient_dob',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['patient_dob'] = $instance->controls->get('date', $args);

		$args = array(
			'class' => 'form-control',
			'name' => 'Age ',
			'id' => 'patient_age',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['patient_age'] = $instance->controls->get('text', $args);
		$args = array(
			'class' => 'form-control',
			'name' => 'Reffered By ',
			'id' => 'patient_reffered_by',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['patient_reffered_by'] = $instance->controls->get('text', $args);

		for ($i = 0; $i < 10; $i++) {
			$args = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'situations[' . $i . '][name]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['situations[' . $i . '][name]'] = $instance->controls->get('text', $args);
		}


		for ($i = 0; $i < 10; $i++) {

			$args1 = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'medications[' . $i . '][taken]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['medications[' . $i . '][taken]'] = $instance->controls->get('text', $args1);



			$args2 = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'medications[' . $i . '][for_what]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['medications[' . $i . '][for_what]'] = $instance->controls->get('text', $args2);


			$args = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'medications[' . $i . '][dosage]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['medications[' . $i . '][dosage]'] = $instance->controls->get('text', $args);


			$args = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'medications[' . $i . '][time]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['medications[' . $i . '][time]'] = $instance->controls->get('text', $args);


			$args = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'medications[' . $i . '][period]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['medications[' . $i . '][period]'] = $instance->controls->get('text', $args);



			$args = array(
				'class' => 'form-control',
				'name' => $i,
				'id' => 'medications[' . $i . '][problems]',
				'value' => '',
				'desc' => '',
				'show_desc' => true,
				'position_desc' => 1, // 1 = Side, 2 = Below,
				'attributes' => array(),
			);
			$return['medications[' . $i . '][problems]'] = $instance->controls->get('text', $args);


		}

		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'under_any_theorapy',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['under_any_theorapy'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'major_surgery',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['major_surgery'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'major_allergy',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['major_allergy'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'emergency_treatment',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['emergency_treatment'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'hospitaliztion',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['hospitaliztion'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'exercise_regularly',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['exercise_regularly'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'major_stress',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['major_stress'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'family_history',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['family_history'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'daily_meal_plan',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);

		$return['daily_meal_plan'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => 'form-control',
			'name' => '',
			'id' => 'scan_file',
			'value' => '',
			'desc' => '',
			'show_desc' => true,
			'position_desc' => 1, // 1 = Side, 2 = Below,
			'attributes' => array(),
		);
		$return['scan_file'] = $instance->controls->get('textarea', $args);
		$args = array(
			'class' => '',
			'name' => '',
			'id' => 'terms',
			'attributes' => array(),
			'choices' => array("term" => "I Agree"),
			'show_label' => true,
		);
		$return['terms'] = $instance->controls->get('checkbox', $args);
		return apply_filters('consultform_variables', $return);

	}

	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	function getTaxonomyList2($key)
	{
		$defaults = array(
			'taxonomy' => $key,
			'hide_empty' => false
		);
		
		// Get the list
		$list = \radient\Radient_Classes_Taxonomy::getList($defaults);
		
		// Return container
		$a = array();
		
		// If there is a list
		if ($list) {
			foreach ($list as $item) {
				//if ( !is_object($item) ) v($item);
				$a[@$item->term_id] = $item->name;
			}
		}

		return $a;
	}

}
