<?php

namespace sharanconsultation;

// Do not allow direct loading of this file.
if ( preg_match('#' . basename(__FILE__) . '#', $_SERVER ['PHP_SELF']) ) {
	exit();
}

class Sharanconsultation_Views_Consultations_View extends \radient\Radient_Classes_View
{

	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	/*function getVariables()
	{
		$return = Sharanconsultation_Init()->settings->getFromArray('view_variables', 'project');
		return apply_filters('sharanconsultation_variables', $return);
	}
	*/
	 public function display($tmpl = 'default')
    {
        if (!is_user_logged_in()) {
            echo __('Sorry, you need to login to access this section');
            return null;
		}
		
		$instance = \radient\Init::get_instance(__NAMESPACE__);
		$model = $instance->get_model('consultations');
		$this->rows = $model->getList();
		
		$this->set_model($model);

        parent::display($tmpl);
    }
}
