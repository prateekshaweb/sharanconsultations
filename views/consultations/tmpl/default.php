<?php

/**
 * sharanconsultation - Edit Screen
 *
 * @category Projects
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

$instance = \radient\Init::get_instance(__NAMESPACE__);
$request = $instance->request;


$model = $this->get_model();
//$order = $model->get_order_by();
//$order_dir = $model->get_order();

/*
// Code for
$date = \radient\Radient_Helpers_Common::sortOrderTitle(array(
    'text' => 'Date',
    'order' => 'post_date',
    'order_dir' => $order_dir,
    'current_order' => $order,
));
$title = \radient\Radient_Helpers_Common::sortOrderTitle(array(
    'text' => 'Title',
    'order' => 'post_title',
    'order_dir' => $order_dir,
    'current_order' => $order,
));
*/

$counter = $this->getCounter();

/*
$buttons = new \radient\Radient_Helpers_Button();
$url = add_query_arg(array(
    'controller' => 'projects',
    'action' => 'add'));

$buttons->addButton(array(
    'href' => $url,
    'text' => __('Add'),
    'class' => 'btn btn-default btn-primary',
));
*/

echo $instance->mvc->menu();
?>
<form id="browser-form">
<?php //echo $buttons->render(); ?>
<div style="width:100%; overflow: hidden; padding-top: 20px">
        <table class="table table-striped table-bordered table-hover table-sm" width="100%">
        <thead>
            <tr>
                <th align="left" style="width: 5%">
                    <?php echo __('No'); ?>
                </th>
				<th align="left" style="width: 20%">
                    <?php echo __('Date'); ?>
                </th>
				<th align="left" style="width: 30%">
                    <?php echo __('Title'); ?>
                </th>
				<th align="left" style="width: 15%">
                    <?php echo __('Order Id'); ?>
                </th>
                <th align="left" style="width: 35%">
                    <?php echo __('Action'); ?>
                </th>
            </tr>
            </thead>
            <tbody>
			<?php
foreach ($this->rows as $row) {
    $view_link = add_query_arg(array(
        'controller' => $this->controller,
        'action' => 'edit',
        'id' => $row->ID));
    ?>
				<tr>
                <th scope="row"><?php echo $counter; ?></th>
					<td><?php echo get_the_date('d-M-Y'); ?></td>
					<td><?php echo $row->post_title; ?></td>
					<td><?php echo get_post_meta($row->ID, 'order_id', true); ?></td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="<?php echo $view_link; ?>" class="btn btn-info btn-sm" role="button">View</a>
                        </div>
                    </td>
				</tr>
				<?php
$counter++;
}
?>
</tbody>
			</table>
			<?php echo $this->pagination; ?>
	</div>

<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="page_id" value="<?php echo $this->page_id; ?>" />
<input type="hidden" name="action" value="<?php echo $this->action; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $order; ?>" />
<input type="hidden" name="order_dir" id="order_dir" value="<?php echo $order_dir; ?>" />
</form>