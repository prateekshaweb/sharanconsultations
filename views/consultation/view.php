<?php

namespace sharanconsultation;

// Do not allow direct loading of this file.
if ( preg_match('#' . basename(__FILE__) . '#', $_SERVER ['PHP_SELF']) ) {
	exit();
}

class Sharanconsultation_Views_Consultation_View extends \radient\Radient_Classes_Viewedit
{
	
	static $_namespace = __NAMESPACE__;

	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	function getVariables()
	{
		$return = Sharanconsultation_Init()->settings->getFromArray('view_variables', 'project');
		return apply_filters('sharanconsultation_variables', $return);
	}
	
	
}
