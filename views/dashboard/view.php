<?php

namespace sharanconsultation;

class Sharanconsultation_Views_Dashboard_View extends \radient\Radient_Classes_View
{

    

    public function display($tmpl = 'default')
    {
        if (!is_user_logged_in()) {
            echo __('Sorry, you need to login to access this section');
            return null;
        }

        parent::display($tmpl);
    }
}
