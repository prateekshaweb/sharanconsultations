<?php

/**
 * Trait for Colour TermMeta
 * This class is used to add Colour Termmeta in Posttypes
 *
 * PHP Version 5.6
 *
 * @category  TermMeta
 * @package   sharanConsultation
 * @author    Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license   see license.txt
 * @link      http://www.prateeksha.com/sharanconsultation/
 */

namespace sharanconsultation;

use \radient\Radient_Helpers_Controls;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Trait for Colour TermMeta
 * This class is used to add Colour Termmeta in Posttypes
 *
 */
trait Sharanconsultation_Termmeta_Colour
{

    /**
     * Method to show the form for element
     * Will echo the form
     *
     * @param string $taxonomy
     *
     * @return void
     */
    public static function add_colour_group_field($taxonomy)
    {
        ?>
        <div class="form-field term-group">
            <label for="colour"><?php _e('Colour', 'sharanconsultation');?></label>
            <?php
            $args = array(
                'class' => 'regular-text',
                'name' => 'colour',
                'id' => 'colour',
                'value' => '',
                'desc' => '',
                'show_desc' => true,
                'position_desc' => 1, // 1 = Side, 2 = Below,
                'attributes' => array(),
            );
            $a = Radient_Helpers_Controls::getInput('colorpicker');
            echo $a->render($args);
            ?>
        </div><?php
    }

    /**
     * Method to show the form for element
     * Will echo the form
     *
     * @param integer $term_id
     * @param integer $tt_id
     * 
     * @return void
     */
    public function save_colour_meta($term_id, $tt_id)
    {
        if (isset($_POST['colour']) && '' !== $_POST['colour']) {

            $args = array(
                'class' => 'fullwidth-text',
                'name' => 'colour',
                'id' => 'colour',
                'desc' => '',
                'show_desc' => true,
                'position_desc' => 1, // 1 = Side, 2 = Below,
                'attributes' => array(),
            );
            $a = Radient_Helpers_Controls::getInput('colorpicker');
            $value = $a->sanitize($args, $_POST['colour']);

            add_term_meta($term_id, 'colour', $group, true);
        }
    }

    public static function edit_colour_group_field($term, $taxonomy)
    {
        // get current group
        $colour = get_term_meta($term->term_id, 'colour', true);

        ?><tr class="form-field term-group-wrap">
        <th scope="row"><label for="colour-group"><?php _e('Colour', 'sharanconsultation');?></label></th>
        <td>
        <?php
$args = array(
            'class' => 'fullwidth-text',
            'name' => 'colour',
            'id' => 'colour',
            'value' => $colour,
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
        );
        $a = Radient_Helpers_Controls::getInput('colorpicker');
        echo $a->render($args);
        ?>
        </td>
    </tr><?php
}

    public static function update_colour_meta($term_id, $tt_id)
    {
        if (isset($_POST['colour']) && '' !== $_POST['colour']) {

            $args = array(
                'class' => 'fullwidth-text',
                'name' => 'colour',
                'id' => 'colour',
                'desc' => '',
                'show_desc' => true,
                'position_desc' => 1, // 1 = Side, 2 = Below,
                'attributes' => array(),
            );
            $a = Radient_Helpers_Controls::getInput('colorpicker');
            $value = $a->sanitize($args, $_POST['colour']);

            update_term_meta($term_id, 'colour', $value);
        }
    }

    public static function add_colour_column($columns)
    {
        $columns['colour'] = __('Colour', 'my_plugin');
        return $columns;
    }

    public static function add_colour_column_content($content, $column_name, $term_id)
    {
        if ($column_name !== 'colour') {
            return $content;
        }

        $term_id = absint($term_id);
        $colour = get_term_meta($term_id, 'colour', true);

        if (!empty($colour)) {
            $content .= esc_attr($colour);
        }

        return $content;
    }

}