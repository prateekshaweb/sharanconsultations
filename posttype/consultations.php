<?php

/**
 * Class for Accounts Post Type
 * Workorder is used for Products, Services etc
 *
 * PHP Version 5.6
 *
 * @category  Posttype
 * @package   sharanConsultation
 * @author    Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license   see license.txt
 * @link      http://www.prateeksha.com/sharanconsultation/
 */

namespace sharanconsultation;

//use \radient\Radient_Helpers_Controls;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Accounts Post Type
 * Accounts is used for Customers, Competitors etc
 *
 */
class Sharanconsultation_Posttype_Consultations extends \radient\Radient_Classes_Posttype
{

    /**
     * Method to register the post type.
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function init($args = array())
    {
        $args = array(
            'classname' => __class__,
            'posttype' => 'consultations',
            'namespace' => __NAMESPACE__,
            'plural' => __('Consultatoins', 'sharanconsultation'),
            'singular' => __('Consultation', 'sharanconsultation'),
            'show_ui' => true,
            'show_in_menu' => false,
            'supports' => array(
                'title',
            ),
            'columns_head' => array(
                'title' => __('Name', 'rdsworkorders'),
                'name' => __('Name', 'rdsworkorders'),
                'invoice_created_date' => __('Date', 'rdsworkorders'),
                'invoice_company' => __('Company', 'rdsworkorders'),
                'status' => __('Status', 'rdsworkorders'),
            ),
            'columns_head_unset' => array('date'),
            'columns_content' => array(
                'invoice_created_date' => array('\rdsworkorders\Rdsworkorders_Helpers_Columncontent', 'invoice_created_date'),
                'name' => array('\rdsworkorders\Rdsworkorders_Helpers_Columncontent', 'name'),
                'invoice_company' => array('\rdsworkorders\Rdsworkorders_Helpers_Columncontent', 'invoice_company'),
                'customer' => array('\rdsworkorders\Rdsworkorders_Helpers_Columncontent', 'customer'),
            ),
        );
        parent::register($args);
    }

    /**
     * Method to add filters
     *
     * @return HTML
     */
    public static function show_filters()
    {
        $args = self::get_args();
        $app = \radient\Init::get_instance($args['namespace']);

        // @todo Add customer dropdown
    }

    /**
     * Method to parse the query for filter.
     *
     * @param object $query Wp_Query object
     *
     * @return Wp_Query object
     */
    public static function parse_query($query)
    {
        $args = self::get_args();
        $app = \radient\Init::get_instance($args['namespace']);

        global $pagenow, $post_type;
        if ($post_type != $args['posttype']) {
            return;
        }

        if (!$query->is_main_query()) {
            return;
        }

        if (!is_admin()) {
            return;
        }

        if ($pagenow != 'edit.php') {
            return;
        }

        $request = $app->request;

        $meta_query = array(
            'relation' => 'AND', // Optional, defaults to "AND"
        );

        $filter_status = $request->get('filter_status');
        if (!empty($filter_status)) {
            $meta_query[] = array(
                'key' => 'rdsinvoicesstatus',
                'value' => $filter_status,
                'compare' => 'LIKE',
            );
        }

        $filter_date = $request->get('filter_date', '', 'string');
        self::parseFiltersDate($filter_date, $meta_query, 'activities_date');

        if ($meta_query) {
            $query->set('meta_query', $meta_query);
        }
        $filter_user_id = $request->get('filter_user_id', 0, 'integer');
        if ($filter_user_id) {
            $query->query_vars['author'] = intval($filter_user_id);
        }
        return apply_filters('rdsworkorders_add_parse_filters', $query);
    }

    public function add_feature_group_field($taxonomy)
    {
        global $feature_groups;
        ?><div class="form-field term-group">
        <label for="featuret-group"><?php _e('Feature Group', 'my_plugin'); ?></label>
        <select class="postform" id="equipment-group" name="feature-group">
            <option value="-1"><?php _e('none', 'my_plugin'); ?></option><?php foreach ($feature_groups as $_group_key => $_group) : ?>
                <option value="<?php echo $_group_key; ?>" class=""><?php echo $_group; ?></option>
            <?php endforeach; ?>
        </select>
    </div><?php

        }

        /**
         * Method to remove taxonomy meta
         *
         * @uses  remove_meta_box
         * @since 1.0
         *
         * @return void
         */
        public static function remove_taxonomy_meta()
        {
            remove_meta_box('tagsdiv-rdsinvoicesstatus', 'rdsinvoices', 'advanced');
        }

        /**
         * Method to add the table sorting
         */
        public static function tableSorting($columns)
        {
            $columns['lead_date'] = 'lead_date';
            $columns['status'] = 'status';
            return $columns;
        }

        public static function column_ordering($vars)
        {
            if (isset($vars['orderby'])) {

                switch ($vars['orderby']) {
                    case 'lead_date':
                        $vars = array_merge($vars, array(
                            'meta_key' => 'lead_date',
                            'orderby' => 'meta_value',
                        ));
                        break;
                }
            }

            return $vars;
        }

    }
