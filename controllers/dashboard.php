<?php

/**
 * File for the class for Controllers for Dashboard
 *
 * @category Controllers
 * @package Prateeksha_Dailylogs
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2017 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Controllers for Dashboard
 *
 *
 */
class Sharanconsultation_Controllers_Dashboard extends \radient\Radient_Classes_Controller
{

}
