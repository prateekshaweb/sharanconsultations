<?php

/**
 * sharanconsultation - Project Management.
 *
 * @category Projects
 *          
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 *         
 * @link http://www.prateeksha.com/
 *      
 */

namespace sharanconsultation;


// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit();
}

class Sharanconsultation_Controllers_Addconsultations extends \radient\Radient_Classes_Controller
{

	public function confirm()
	{
		extract($this->_args);

		global $wpdb;

		$instance = \radient\Init::get_instance(__NAMESPACE__);
		$request = $instance->request;

		$view = $this->getView($view);

		$model = $instance->get_model('consultations');
		$view->set_model($model);

		$view->display('confirm');
	}
	public function save($args = array())
	{

		$current_user_id = get_current_user_id();
		if (!$current_user_id) {
			die('Unauthorised access');
		}

		$instance = \radient\Init::get_instance(__NAMESPACE__);
		$request = $instance->request;

		$postmeta = array();

		/**
		 * Casting is required in Mysql to get integer values https://wordpress.stackexchange.com/questions/54794/select-maxmeta-value-from-wp-postmeta-where-meta-key-price-stops-working
		 */


		$postmeta['created_date'] = date("d-m-Y");

        // Take the information from the current user
		$current_user = wp_get_current_user();

		$postmeta['user_id'] = $current_user_id;
		$postmeta['patient_fname'] = $request->post('patient_fname', '', 'string');
		$postmeta['patient_lname'] = $request->post('patient_lname', '', 'string');
		$postmeta['patient_gender'] = $request->post('patient_gender', '', 'string');
		$postmeta['patient_address'] = $request->post('patient_address', '', 'string');
		$postmeta['patient_phone'] = $request->post('patient_phone', '', 'string');
		$postmeta['patient_email'] = $request->post('patient_email', '', 'string');
		$postmeta['patient_dob'] = $request->post('patient_dob', '', 'string');
		$postmeta['patient_age'] = $request->post('patient_age', '', 'string');
		$postmeta['patient_reffered_by'] = $request->post('patient_reffered_by', '', 'string');

		$postmeta['patient_dob'] = $request->post('patient_dob', '', 'string');

		$postmeta['under_any_theorapy'] = $request->post('under_any_theorapy', '', 'string');
		$postmeta['major_surgery'] = $request->post('major_surgery', '', 'string');
		$postmeta['major_allergy'] = $request->post('major_allergy', '', 'string');
		$postmeta['daily_meal_plan'] = $request->post('daily_meal_plan', '', 'string');
		$postmeta['major_stress'] = $request->post('major_stress', '', 'string');
		$postmeta['emergency_treatment'] = $request->post('emergency_treatment', '', 'string');
		$postmeta['hospitaliztion'] = $request->post('hospitaliztion', '', 'string');
		$postmeta['exercise_regularly'] = $request->post('exercise_regularly', '', 'string');
		$postmeta['family_history'] = $request->post('family_history', '', 'string');
		$postmeta['situations'] = $request->post('situations',array(), 'array');
		$postmeta['medications'] = $request->post('medications', array(), 'array');
		$postmeta['scan_file'] = $request->post('scan_file', '', 'string');
		$postmeta['terms'] = $request->post('terms', '', 'string');
		$new_post = array(
			'post_title' => sprintf($postmeta['patient_fname'] . " " . $postmeta['patient_lname']),
			'post_content' => '',
			'post_status' => 'publish',
			'post_type' => 'consultations',
		);

        //insert the the post into database by passing $new_post to wp_insert_post
        //store our post ID in a variable $post_id
		$post_id = wp_insert_post($new_post);
		if (!$post_id) {
			die('could not create post');
		}
		$instance->postmeta->save($post_id, $postmeta);

	}

}
