<?php

/**
 * Plugin Name: Sharan Consultation
 * Plugin URI:  http://www.prateeksha.com/
 * Description: An Custom Management plugin for WordPress.
 * Version:     1.04
 * Author:      Prateeksha
 * Author URI:  http://www.prateeksha.com/
 * Text Domain: sharanconsultation
 * License:     GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
 * Domain Path: /languages
 *
 */

namespace sharanconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

define(__NAMESPACE__ . '\SHARANCONSULTATION_NAME', 'sharanconsultation');
define(__NAMESPACE__ . '\SHARANCONSULTATION_VERSION', '1.04');
define(__NAMESPACE__ . '\SHARANCONSULTATION_SLUG', basename(dirname(__FILE__)));
define(__NAMESPACE__ . '\SHARANCONSULTATION_POSTMETA', 'sharanconsultation');
define(__NAMESPACE__ . '\SHARANCONSULTATION_DIR', trailingslashit(plugin_dir_path(__FILE__)));
define(__NAMESPACE__ . '\SHARANCONSULTATION_URI', trailingslashit(plugin_dir_url(__FILE__)));
define(__NAMESPACE__ . '\SHARANCONSULTATION_OPTIONS_NAME', 'sharanconsultation_settings');
define(__NAMESPACE__ . '\SHARANCONSULTATION_FILE', __FILE__);


$file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'radient' . DIRECTORY_SEPARATOR . 'init.php';
if (!file_exists($file)) {

    function adminNotices()
    {
        echo sprintf('<div class="error notice">
				<p>%s</p>
				</div>', __(\esc_html(strip_tags('Consultation App requires Radient Plugin to be installed. Click here to download the framework')), SHARANCONSULTATION_TEXTDOMAIN));
    }

    add_action('admin_notices', '\sharanconsultation\adminNotices');
    return true;
}


// include the main init file
include_once $file;

/**
 * Main Application
 */
$instance = \radient\Init::get_instance(__NAMESPACE__);
$instance->configmanager->setPath(SHARANCONSULTATION_DIR, SHARANCONSULTATION_URI);
$instance->check_php_version(5.3);
$instance->addPath(SHARANCONSULTATION_DIR);
$instance->template->set_path(SHARANCONSULTATION_DIR, 'templates');
$instance->configmanager->load_file(SHARANCONSULTATION_DIR.'config.json');
$instance->add_action('show_user_profile', array('\sharanconsultation\Sharanconsultation_Helpers_Userprofile', 'extra_user_profile_fields'), true);
$instance->add_action('edit_user_profile', array('Sharanconsultation_Helpers_Userprofile', 'extra_user_profile_fields'), true);
$instance->add_action('personal_options_update', array('\sharanconsultation\Sharanconsultation_Helpers_Userprofile', 'save_extra_user_profile_fields'), true);
$instance->add_action('edit_user_profile_update', array('\sharanconsultation\Sharanconsultation_Helpers_Userprofile', 'save_extra_user_profile_fields'), true);

///$instance->common->add_top_button(array('sharanconsultation'));
/*$instance->add_action('current_screen', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'add_buttons_to_top'));*/
$instance->add_action('admin_menu', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'admin_menu'));
/*$instance->add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'view_workorder'));
$instance->add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'view_workorder_pdf'));*/
$instance->add_action('plugins_loaded', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'load_language'));
/*
$instance->add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'view_invoice'));
$instance->add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Helpers_Misc', 'view_invoice_pdf'));*/

$instance->add_action("wp_ajax_send_workorder_email", array('\sharanconsultation\Sharanconsultation_Helpers_Ajax', "send_workorder_email"), true);
$instance->add_action("wp_ajax_nopriv_send_workorder_email", array('\sharanconsultation\Sharanconsultation_Helpers_Ajax', "send_workorder_email"), true);

// Shortcode
add_shortcode('consultform_app', array($instance->mvc, 'render'));

//$instance->add_action('admin_init', array('\sharanconsultation\Sharanconsultation_Adminpages_Settings', 'render'));

$instance->settingspage = new \radient\Radient_Helpers_Pagesettings(new Sharanconsultation_Adminpages_Pageoptions);

add_action("admin_menu", array($instance->settingspage, "add_new_menu_items"));
add_action("admin_init", array($instance->settingspage, "display_options"));

$instance->wp_localize_script( 'rds-front-js"', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );



if (!function_exists('\sharanconsultation\v')) {
    function v()
    {
        $a = func_get_args();
        if ($a) {
            foreach ($a as $value) {
                echo '<pre>';
                var_dump($value);
                echo '</pre>';
            }
        }
    }
}




return ;




/**
 * Main Application
 */

$instance = \radient\Init::get_instance(__NAMESPACE__);

/**
 * This is a plugin for admin automatic updates.
 * wp-updates.com
 */
require_once 'wp-updates-plugin.php';
new \WPUpdatesPluginUpdater_1821('http://wp-updates.com/api/2/plugin', plugin_basename(__FILE__));

// Init the main application
include_once $file;
include_once SHARANCONSULTATION_DIR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'init.php';

if (!function_exists('\sharanconsultation\v')) {
    function v()
    {
        $a = func_get_args();
        if ($a) {
            foreach ($a as $value) {
                echo '<pre>';
                var_dump($value);
                echo '</pre>';
            }
        }
    }
}

/**
 * Main Application
 * we have a main application which acts like a Factory
 */
$a = Sharanconsultation_Init();

add_filter('wp_dropdown_users_args', 'sharanconsultation\add_subscribers_to_dropdown', 10, 2);
function add_subscribers_to_dropdown($query_args, $r)
{

    $query_args['who'] = '';
    return $query_args;

}



add_action('show_user_profile', 'sharanconsultation\extra_user_profile_fields');
add_action('edit_user_profile', 'sharanconsultation\extra_user_profile_fields');

function extra_user_profile_fields($user)
{?>
<h3><?php _e("Extra profile information", "blank");?></h3>

<table class="form-table">

<tr>
<th><label for="province"><?php _e("Accounts List  : ");?></label></th>
<td>
 <?php $args = array(
    'post_type' => 'crmaccounts',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'post_title',
    'order' => 'ASC',
);

$results = get_posts($args);
?>
        <select id="account_name" name="account_name">
        <?php
foreach ($results as $result) {?>
<option value="<?php echo $result->post_title; ?>"  <?php 
if(get_the_author_meta( 'account_name', $user->ID )==$result->post_title)
{echo " selected";}
?>
>
   <?php echo $result->post_title;  ?></option>
<?php }?>
</select>
</td>
   
</tr>

</table>
<?php }

add_action('personal_options_update', 'sharanconsultation\save_extra_user_profile_fields');
add_action('edit_user_profile_update', 'sharanconsultation\save_extra_user_profile_fields');

function save_extra_user_profile_fields($user_id)
{

    if (!current_user_can('edit_user', $user_id)) {return false;}

    update_user_meta($user_id, 'account_name', $_POST['account_name']);


}



function myFunction()
{




// ----------------ADD POST  front End
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == "my_post_type") {
    
    $request = Sharanconsultation_Init()->request;
    
    if (isset ($_POST['fname'])|| isset ($_POST['lname'])) {
      $fname =  $_POST['fname'];
  } else {
      echo 'Please enter a Full name';
  }
  if (isset ($_POST['patient_gender'])) {
      $patient_gender = $_POST['patient_gender'];
  } else {
      echo 'Please mention your gender';
  }
      //store our post vars into variables for later use
      //now would be a good time to run some basic error checking/validation
      //to ensure that data for these values have been set
      
      $postmeta = array();
      
      $postmeta['fname']     = $request->post('fname');
      $lname     = \radient\Radient_Helpers_Filter::clean($_POST['lname']);
      $patient_gender   = \radient\Radient_Helpers_Filter::clean($_POST['patient_gender']);
      $patient_address   = \radient\Radient_Helpers_Filter::clean($_POST['patient_address']);
      $patient_address   = \radient\Radient_Helpers_Filter::clean($_POST['patient_address']);
      $patient_email   = \radient\Radient_Helpers_Filter::clean($_POST['patient_email']);
      $patient_dob   = \radient\Radient_Helpers_Filter::clean($_POST['patient_dob']);
      $patient_age   = \radient\Radient_Helpers_Filter::clean($_POST['patient_age']);
      $patient_reffered_by   = \radient\Radient_Helpers_Filter::clean($_POST['patient_reffered_by']);
      
      
      $postmeta['situations'] = $request->post('situations', array(), 'array');
      
      for($i=1;$i<=10;$i++)
      {
          if(isset ($_POST['situation'.$i]))
          {
            $situation[$i]   = \radient\Radient_Helpers_Filter::clean($_POST['situation'.$i]);
          }
      }

	  $medications = $request->post('medications', array(), 'array');
	
      for($i=0;$i<10;$i++)
      {
        $defaults = array(
            'taken' => '',
            'for_what' => '',
            'dosage' => '',
            'time' => '',
            'period'=>'',
            'problems' => '',
           
        );
        $facility = wp_parse_args($facility, $defaults);
        $facility['taken'] = \radient\Radient_Helpers_Filter::clean($_POST['medications[0][taken]']);
        $facility['for_what'] = \radient\Radient_Helpers_Filter::clean($_POST['medications[0][for_what]']);
        $facility['dosage'] = \radient\Radient_Helpers_Filter::clean($_POST['medications[0][dosage]']);
        $facility['time'] = \radient\Radient_Helpers_Filter::clean($_POST['medications[0][time]']); 
        $facility['period'] = \radient\Radient_Helpers_Filter::clean($_POST[' medications[0][period]']);
        $facility['problems'] = \radient\Radient_Helpers_Filter::clean($_POST['medications[0][problems]']);
        $medications[$i] = $facility;
      }

      $under_any_theorapy   = \radient\Radient_Helpers_Filter::clean($_POST['under_any_theorapy']);
      $major_surgery   = \radient\Radient_Helpers_Filter::clean($_POST['major_surgery']);
      $major_allergy   = \radient\Radient_Helpers_Filter::clean($_POST['major_allergy']);
      $emergency_treatment   = \radient\Radient_Helpers_Filter::clean($_POST['emergency_treatment']);
      $hospitaliztion   = \radient\Radient_Helpers_Filter::clean($_POST['hospitaliztion']);
      $exercise_regularly   = \radient\Radient_Helpers_Filter::clean($_POST['exercise_regularly']);
      $major_stress   = \radient\Radient_Helpers_Filter::clean($_POST['major_stress']);
      $family_history   = \radient\Radient_Helpers_Filter::clean($_POST['family_history']);
      $daily_meal_plan   = \radient\Radient_Helpers_Filter::clean($_POST['daily_meal_plan']);
      //$post_type = 'my_custom_post';
      //$custom_field_1 = $_POST['custom_1'];
      //$custom_field_2 = $_POST['custom_2'];    
    
      //the array of arguements to be inserted with wp_insert_post
      $new_post = array(
      'post_title'    => $fname.' '.$lname,
      'post_content'  => '',
      'post_status'   => 'publish',          
      'post_type'     =>'consultations' 
      );
     
      //insert the the post into database by passing $new_post to wp_insert_post
      //store our post ID in a variable $pid
  
     
      $pid = wp_insert_post($new_post);
  
      //we now use $pid (post id) to help add out post meta data
      add_post_meta($pid, 'patient_fname', $fname, true);
      add_post_meta($pid, 'patient_lname', $lname, true);
      add_post_meta($pid, 'patient_gender', $patient_gender, true);
      add_post_meta($pid, 'patient_address', $patient_address, true);
      add_post_meta($pid, 'patient_phone', $patient_address, true);
      add_post_meta($pid, 'patient_email', $patient_email, true);
      add_post_meta($pid, 'patient_dob', $patient_dob, true);
      add_post_meta($pid, 'patient_age', $patient_age, true);
      add_post_meta($pid, 'patient_reffered_by', $patient_reffered_by, true);
      
      for($i=1;$i<=10;$i++)
      {
          if(isset ($_POST['situation'.$i]))
          {
      add_post_meta($pid, 'situation'.$i, $situation[$i], true);
            }
        }
  
        add_post_meta($pid, 'under_any_theorapy', $under_any_theorapy, true);
        add_post_meta($pid, 'major_surgery', $major_surgery, true);
        add_post_meta($pid, 'major_allergy', $major_allergy, true);
        add_post_meta($pid, 'emergency_treatment', $emergency_treatment, true);
        add_post_meta($pid, 'hospitaliztion', $hospitaliztion, true);
        add_post_meta($pid, 'exercise_regularly', $exercise_regularly, true);
        add_post_meta($pid, 'major_stress', $major_stress, true);
        add_post_meta($pid, 'family_history', $family_history, true);
        add_post_meta($pid, 'daily_meal_plan', $daily_meal_plan, true);
      
      }

}
add_action('init', 'sharanconsultation\myFunction');

